﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;
using Blockchain_New.Models;
using System.Net.Http.Formatting;
using System.Collections.Generic;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Web.UI.WebControls;
using System.Runtime.CompilerServices;
using System.Linq;
using System.Text.RegularExpressions;

namespace Blockchain_New
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonSubmit_OnClick(object sender, EventArgs e)
        {
            string extention_file = System.IO.Path.GetExtension(FileUploadLampiran.FileName);
            string[] list_extention = { ".jpg", ".jpeg", ".png", ".pdf", ""};
            var email = new Regex(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");

            if (!list_extention.Contains(extention_file))
            {
                LabelBerhasil.Visible = true;
                LabelBerhasil.Text = "Your file must an image or a pdf.";
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
            }
            else if (!email.IsMatch(TextBoxEmail.Text))
            {
                LabelBerhasil.Visible = true;
                LabelBerhasil.Text = "Your email is not valid.";
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
            }
            else
            {
                InputData();
            }
        }

        public string uploading_file()
        {
            if (FileUploadLampiran.HasFile)
            {
                string File = FileUploadLampiran.FileName.ToString();
                string extention = System.IO.Path.GetExtension(File);
                if (extention == ".pdf")
                {
                    FileUploadLampiran.PostedFile.SaveAs(Server.MapPath("~/Lampiran/PDF/" + File));
                }
                else
                {
                    FileUploadLampiran.PostedFile.SaveAs(Server.MapPath("~/Lampiran/Image/" + File));
                }       
                return File;
            }
            else
            {
                string File = "";
                return File;
            };
        }
        public void InputData()
        {
            string File = uploading_file();
            try
            {
                using (var client = new HttpClient())
                {
                    Laporan report = new Laporan
                    {
                        namaDepan = TextBoxNamaDepan.Text,
                        namaBelakang = TextBoxNamaBelakang.Text,
                        email = TextBoxEmail.Text,
                        tanggalLaporan = DateTime.Today,
                        judulLaporan = TextBoxSubject.Text,
                        isiLaporan = TextareaIsi.Value,
                        lampiranLaporan = File
                    };

                    client.BaseAddress = new Uri("http://localhost:9999/laporan/");
                    var response = client.PostAsJsonAsync("tambahLaporan", report).Result;
                    var content = response.Content.ReadAsStringAsync().Result;

                    if (response.IsSuccessStatusCode)
                    {
                        TextBoxNamaDepan.Text = string.Empty;
                        TextBoxNamaBelakang.Text = string.Empty;
                        TextBoxEmail.Text = string.Empty;
                        TextBoxSubject.Text = string.Empty;
                        TextareaIsi.Value = string.Empty;
                        FileUploadLampiran.Attributes.Clear();

                        LabelBerhasil.Visible = true;
                        LabelBerhasil.ForeColor = System.Drawing.Color.Green;
                        LabelBerhasil.Text = "Your report is successfully input.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LabelBerhasil.Visible = true;
                LabelBerhasil.Text = ex.Message;
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
            }
        }
    }
}