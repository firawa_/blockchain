﻿using System;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;
using Blockchain_New.Models;
using System.Net.Http.Formatting;
using System.Collections.Generic;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

namespace Blockchain_New
{
    public partial class AccountsettingSuperadmin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["email"] == null && Session["role"] == null)
            {
                logout();
            }
            else if (Session["role"].ToString() == "PERUSAHAAN")
            {
                Response.Redirect("Homeperusahaan.aspx");
            }
            else if (Session["role"].ToString() == "ADMIN")
            {
                Response.Redirect("Home2.aspx");
            }
            else if (Session["role"].ToString() == "INSTANSI")
            {
                Response.Redirect("Home4.aspx");
            }
            else if (Session["role"].ToString() == "USER")
            {
                Response.Redirect("Home.aspx");
            }
        }

        public void logout()
        {
            string email = GlobalVariable.email;
            try
            {
                using (var client = new HttpClient())
                {
                    Users logout = new Users
                    {
                        email = email
                    };

                    client.BaseAddress = new Uri("http://localhost:9999/users/");
                    var response = client.PostAsJsonAsync("logout", logout).Result;
                    var content = response.Content.ReadAsStringAsync().Result;

                    if (response.IsSuccessStatusCode)
                    {
                        Session.Abandon();
                        Response.Redirect("Login.aspx");
                    }

                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void ButtonUbah_Click(object sender, EventArgs e)
        {
            string email = Session["email"].ToString();

            Users users = new Users
            {
                email = email,
                password = TextBoxPasswordlama.Text
            };

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:9999/users/");
                var response = client.PostAsJsonAsync("cekUserAda", users).Result;
                var content = response.Content.ReadAsStringAsync().Result;

                if (response.IsSuccessStatusCode)
                {
                    System.Diagnostics.Debug.WriteLine(content);
                    System.Diagnostics.Debug.WriteLine(content.Trim());
                    try
                    {
                        using (var client2 = new HttpClient())
                        {
                            Users account = new Users
                            {
                                email = email,
                                password = TextBoxPasswordbaru.Text
                            };

                            var hasNumber = new Regex(@"[0-9]+");
                            var hasUpperChar = new Regex(@"[A-Z]+");
                            var hasMiniMaxChars = new Regex(@".{8,32}");
                            var hasLowerChar = new Regex(@"[a-z]+");
                            var hasSymbols = new Regex(@"[!@#$%^&*()_+=\[{\]};:<>|./?,-]");

                            var pass = TextBoxPasswordbaru.Text;

                            if (!hasLowerChar.IsMatch(pass))
                            {
                                LabelBerhasil.Visible = true;
                                LabelBerhasil.Text = "Your password should contain at least one lower case letter.";
                                ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                            }
                            else if (!hasUpperChar.IsMatch(pass))
                            {
                                LabelBerhasil.Visible = true;
                                LabelBerhasil.Text = "Your password should contain at least one upper case letter.";
                                ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                            }
                            else if (!hasMiniMaxChars.IsMatch(pass))
                            {
                                LabelBerhasil.Visible = true;
                                LabelBerhasil.Text = "Your password should not be less than 8 characters.";
                                ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                            }
                            else if (!hasNumber.IsMatch(pass))
                            {
                                LabelBerhasil.Visible = true;
                                LabelBerhasil.Text = "Your password should contain at least one numeric value.";
                                ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                            }
                            else if (!hasSymbols.IsMatch(pass))
                            {
                                LabelBerhasil.Visible = true;
                                LabelBerhasil.Text = "Your password should contain at least one special case characters.";
                                ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                            }
                            else
                            {
                                if (TextBoxPasswordbaru.Text.Trim() == TextBoxPasswordulangi.Text.Trim())
                                {
                                    client2.BaseAddress = new Uri("http://localhost:9999/users/");
                                    var response2 = client2.PostAsJsonAsync("gantiPasswordUser", account).Result;
                                    var content2 = response2.Content.ReadAsStringAsync().Result;

                                    if (response2.IsSuccessStatusCode)
                                    {
                                        LabelBerhasil.Visible = true;
                                        LabelBerhasil.ForeColor = System.Drawing.Color.Green;
                                        LabelBerhasil.Text = "Your password is successfully to change.";
                                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                                    }
                                    else
                                    {
                                        LabelBerhasil.Visible = true;
                                        LabelBerhasil.Text = "Your new password must differ from the old password.";
                                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                                    }
                                }
                                else
                                {
                                    LabelBerhasil.Visible = true;
                                    LabelBerhasil.Text = "Your new password and confirm password is not the same.";
                                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LabelBerhasil.Visible = true;
                        LabelBerhasil.Text = ex.Message;
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                }
                else if (!response.IsSuccessStatusCode)
                {
                    LabelBerhasil.Visible = true;
                    LabelBerhasil.Text = "Wrong password!";
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                }
            }
        }
    }
}