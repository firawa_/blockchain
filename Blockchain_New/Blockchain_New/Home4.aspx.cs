﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;
using Blockchain_New.Models;
using System.Net.Http.Formatting;
using System.Collections.Generic;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Web.UI.WebControls;

namespace Blockchain_New
{
    public partial class Home4 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["email"] == null && Session["role"] == null)
            {
                logout();
            }
            else if (Session["role"].ToString() == "ADMIN")
            {
                Response.Redirect("Home2.aspx");
            }
            else if (Session["role"].ToString() == "SUPERADMIN")
            {
                Response.Redirect("Home3.aspx");
            }
            else if (Session["role"].ToString() == "USER")
            {
                Response.Redirect("Home.aspx");
            }
            else if (Session["role"].ToString() == "PERUSAHAAN")
            {
                Response.Redirect("Homeperusahaaan.aspx");
            }
            else
            {
                loadmygrid();
            }
        }

        public void logout()
        {
            string email = GlobalVariable.email;
            try
            {
                using (var client = new HttpClient())
                {
                    Users logout = new Users
                    {
                        email =  email
                    };

                    client.BaseAddress = new Uri("http://localhost:9999/users/");
                    var response = client.PostAsJsonAsync("logout", logout).Result;
                    var content = response.Content.ReadAsStringAsync().Result;

                    if (response.IsSuccessStatusCode)
                    {
                        Session.Abandon();
                        Response.Redirect("Login.aspx");
                    }

                }
            }
            catch (Exception ex)
            {

            }
        }

        public void loadmygrid()
        {
            string instansi_name = GlobalVariable.name;
            string pic_name = Session["email"].ToString();
            try
            {
                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri("http://localhost:9999/blockchain/");
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage rs = clients.GetAsync("cariBlockchainByInstansi?instansiName=" + instansi_name + "&picEmail=" + pic_name +"").Result;
                if (rs.IsSuccessStatusCode)
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                    Data.DataSource = (new JavaScriptSerializer()).Deserialize<List<Blockchain>>(s);
                    Data.DataBind();
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void Detail_Click(object Sender, EventArgs e)
        {
            string email = Session["email"].ToString();

            if (email != null)
            {
                LinkButton detailbutton = Sender as LinkButton;

                GridViewRow record = detailbutton.NamingContainer as GridViewRow;

                Session["id"] = record.Cells[0].Text;

                Response.Redirect("Detail2.aspx");
            }
        }
    }
}