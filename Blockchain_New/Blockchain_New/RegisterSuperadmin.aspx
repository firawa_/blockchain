﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegisterSuperadmin.aspx.cs" Inherits="Blockchain_New.RegisterSuperadmin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Register Super Admin</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <link href="Registerstyle.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link rel="Icon" href="Aset/favicon.png" type="image/png" /> 
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"/>
    <script type="text/javascript">
        function HideLabel()
        {
            var seconds = 5;
            setTimeout
                (function () {
                    document.getElementById("<%=Label1RegisterSuperadmin.ClientID %>").style.display = "none";
                }, seconds * 1000);
        };
    </script>
</head>
<body>
    <form id="form1" runat="server">
<br /> 
<div style="align: justify; color:white; width:300px; margin-left:1.2%; font-size:16px;">
<img height="100" src="Aset/Logo.jpeg" style="float: left; margin-right:9px; height:60px" width="80" /><p style="padding-top:6.8%; margin-bottom:0px!important">Blockchain based <br />Certificate Management</p></div>
<br /> 

      <div class="containerr">
        <div class="row">
          <div class="col-md-8">
          </div>

          <div class="col-md-4">
            <center style="color:white; font-size:19px;">Register Superadmin</center>
            <div class="panel panel-default">
            </div>

            <div class="panel panel-default" style="border-radius:20px !important">
              <div class="panel-body">
                  <div class="back">
                      <div class="dalam">

                        <img class="card-img-top" src="Aset/orang.png" alt="Card image cap"/>
                        <br />

                        <div class="row">
                          <div class="col-25">
                            <label for="Namasuperadmin">Nama&nbsp;Super&nbsp;Admin</label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-75">
                            <asp:TextBox Style="text-transform:uppercase;" CssClass="form-control" ID="TextBoxNamasuperadmin" runat="server" required MaxLength="40"></asp:TextBox>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-25">
                            <label for="Username">E-mail</label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-75">
                            <asp:TextBox CssClass="form-control" ID="TextBoxUsername" runat="server" required MaxLength="40"></asp:TextBox>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-25">
                            <label for="Password">Password</label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-75">
                            <asp:TextBox CssClass="form-control" ID="TextBoxPassword" runat="server" TextMode="Password" required></asp:TextBox>
                          </div>
                        </div>
						<p>
							<asp:Label ID="Label1RegisterSuperadmin" runat="server"></asp:Label>
						</p>

                        <br />
                        <div class="row">
							<asp:Button CssClass="btn btn-primary" style="margin-left:5%" ID="ButtonRegister" runat="server" Text="Register" OnClick="ButtonRegister_Click" />
							<a href="Login.aspx" style="margin-left:50%" class="card-link">Login</a>
                        </div>
                      </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
</body>
</html>
