﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;
using Blockchain_New.Models;
using System.Net.Http.Formatting;
using System.Collections.Generic;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Web.UI.WebControls;
using System.IO;
using System.Reflection.Emit;

namespace Blockchain_New
{
    public partial class ForgetPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["status"] == "1")
            {
                LabelBerhasil.Visible = true;
                LabelBerhasil.Text = "Your password reset token has expired.";
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
            }
        }

        protected void ButtonSearch_Click(object sender, EventArgs e)
        {
            sendemail();
        }

        public void sendemail()
        {
            string token = getToken();
            string name = getName().ToLower();
            if (token != "Failed" && name != "Failed")
            {
                //string contents = "<html><head><title>Hello</title><meta charset =\"utf-8\"/><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\"/></head><body><div class=\"container\"><div class=\"row\"><div style=\"text-align:center; font-size:18px\" class=\"col-md-12\"><br /><br /><br /><br />Hello "+ name + "<p>Someone, hopefully you, has requested to reset the password for your account on <a>https://banksertifikat.com</a></p><p>If you did not perform this request, you can safely ignore this email</p><p>Otherwise, click the link below to complete the process.</p><a href=http://localhost:50252/ResetPassword.aspx?token=" + token + "> Reset Password</a><br/><p style=\"font-size:13px\">This link will expire in 1 hours. Please use this link soon.</p></div></div></div></body></html>";

                string contents = "<html><head><title>Hello</title><meta charset =\"utf-8\"/><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\"/></head><body><div class=\"container\"><div class=\"row\"><div style=\"text-align:center; font-size:18px\" class=\"col-md-12\"><br /><br /><br /><br />Hello "+ name + "<p>Someone, hopefully you, has requested to reset the password for your account on <a>https://banksertifikat.com</a></p><p>If you did not perform this request, you can safely ignore this email</p><p>Otherwise, click the link below to complete the process.</p><a href=https://banksertifikat.com/ResetPassword.aspx?token=" + token + "> Reset Password</a><br/><p style=\"font-size:13px\">This link will expire in 1 hours. Please use this link soon.</p></div></div></div></body></html>";
                try
                {
                    using (var client = new HttpClient())
                    {
                        Email mailaddres = new Email
                        {
                            mailTo = TextBoxEnter.Text,
                            mailSubject = "Reset Password Instructions",
                            mailContent = contents
                        };

                        client.BaseAddress = new Uri("http://localhost:9999/mail/");
                        var response = client.PostAsJsonAsync("sendMail", mailaddres).Result;
                        var content = response.Content.ReadAsStringAsync().Result;

                        if (response.IsSuccessStatusCode)
                        {
                            LabelBerhasil.Visible = true;
                            LabelBerhasil.ForeColor = System.Drawing.Color.Green;
                            LabelBerhasil.Text = "The link for reset password has been send to your Email. Please check your Email.";
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                            TextBoxEnter.Text = string.Empty;
                        }
                        else
                        {
                            LabelBerhasil.Visible = true;
                            LabelBerhasil.ForeColor = System.Drawing.Color.Red;
                            LabelBerhasil.Text = "Failed to send the link for reset password.";
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                        }
                    }
                }
                catch (Exception ex)
                {
                    LabelBerhasil.Visible = true;
                    LabelBerhasil.Text = ex.Message;
                }
            }
            else
            {
                LabelBerhasil.Visible = true;
                LabelBerhasil.Text = "User not found";
            }
        }

        public string getToken()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Users email = new Users
                    {
                        email = TextBoxEnter.Text
                    };

                    client.BaseAddress = new Uri("http://localhost:9999/users/password/");
                    var response = client.PostAsJsonAsync("getToken", email).Result;
                    var content = response.Content.ReadAsStringAsync().Result;

                    if (response.StatusCode.ToString() == "NotFound")
                    {
                        return "Failed";
                    }
                    else
                    {
                        string token = content;
                        return token;
                    }
                };
            }
            catch (Exception ex)
            {
                LabelBerhasil.Visible = true;
                LabelBerhasil.Text = ex.Message;
                return "Failed";
            }
        }

        public string getName()
        {
            try
            {
                string email = TextBoxEnter.Text.Trim();
                string uri = "http://localhost:9999/";
                string parameter = "users/cariUsers?email=" + email + "";
                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri(uri);
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = clients.GetAsync(parameter).Result;

                if (response.IsSuccessStatusCode)
                {
                    var content = response.Content.ReadAsStringAsync().Result;
                    var userdata = JsonConvert.DeserializeObject<Users>(content);

                    string name = userdata.name;
                    return name;
                }
                else
                {
                    return "Failed";
                }
            }
            catch (Exception ex)
            {
                LabelBerhasil.Visible = true;
                LabelBerhasil.Text = ex.Message;
                return "Failed";
            }
        }
    }
}