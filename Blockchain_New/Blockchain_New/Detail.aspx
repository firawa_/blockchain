﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="Detail.aspx.cs" Inherits="Blockchain_New.Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Detail Page</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <link href="Detailstyle.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"/>
    <script src="https://kit.fontawesome.com/16b5cf1a92.js" crossorigin:"anonymous"></script>
    <script type="text/javascript">
        function DisplayImageInNewWindow()
        {
            var Image = document.getElementById('<%= ImageScanIjazah.ClientID %>').src;

            html = "<HTML><HEAD><TITLE>Photo</TITLE>"
                + "</HEAD><BODY LEFTMARGIN=0 "
                + "MARGINWIDTH=0 TOPMARGIN=0 MARGINHEIGHT=0><CENTER>"
                + "<IMG src='"
                + Image
                + "' BORDER=0 NAME=image "
                + "onload='window.resizeTo(document.image.width,document.image.height)'>"
                + "</CENTER>"
                + "</BODY></HTML>";
            popup = window.open('', 'image', 'toolbar=0,location=0,directories=0,menuBar=0,scrollbars=0,resizable=1');
            popup.document.open();
            popup.document.write(html);
            popup.document.focus();
            popup.document.close();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<h2 style="text-align:center"><b>Form Detail</b></h2>
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
                Gelar Depan
                <asp:TextBox runat="server" value="" CssClass="form-control" ID="TextBoxGelardepan" ReadOnly="true"></asp:TextBox>
            </div>
            <div class="form-group">
                Nama Lengkap
                <asp:TextBox runat="server" value="" CssClass="form-control" ID="TextBoxNamalengkap" ReadOnly="true"></asp:TextBox>
            </div>
            <div class="form-group">
                Gelar Belakang
                <asp:TextBox runat="server" value="" CssClass="form-control" ID="TextBoxGelarbelakang" ReadOnly="true"></asp:TextBox>
            </div>
            <div class="form-group">
                Tempat, Tanggal lahir
                <asp:TextBox runat="server" value="" CssClass="form-control" ID="TextBoxTTL" ReadOnly="true"></asp:TextBox>
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
                Instansi
                <asp:TextBox runat="server" value="" CssClass="form-control" ID="TextBoxUniversitas" ReadOnly="true"></asp:TextBox>
            </div>
            <div class="form-group">
                Nomor Ijazah
                <asp:TextBox runat="server" value="" CssClass="form-control" ID="TextBoxIjazah" ReadOnly="true"></asp:TextBox>
            </div>
            <div class="form-group">
                Tanggal Dikeluarkan Ijazah
                <asp:TextBox runat="server" value="" CssClass="form-control" ID="TextBoxTanggaldikeluarkanijazah" ReadOnly="true"></asp:TextBox>
            </div>
            <div class="form-group">
                Tahun Terbit Ijazah
                <asp:TextBox runat="server" value="" CssClass="form-control" ID="TextBoxTahunterbitijazah" ReadOnly="true"></asp:TextBox>
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
                Scan Ijazah
                <p>
                <asp:ImageButton ID ="ImageScanIjazah" runat="server" OnClientClick = "DisplayImageInNewWindow(); return false" ReadOnly="true"></asp:ImageButton>
                </p>
                <asp:Label ID="Labelsukses" runat="server" CssClass="mylabel" Text="test" Visible="false"></asp:Label>
                <asp:Button ID="Button1" CssClass="mybutton" runat="server" Text="Open PDF" Visible="false" OnClick="Button1_Click"/>
            </div>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
          </div>
        </div>
      </div>
</asp:Content>
