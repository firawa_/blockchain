﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;
using Blockchain_New.Models;
using System.Net.Http.Formatting;
using System.Collections.Generic;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
namespace Blockchain_New
{
    public partial class Himbauan : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["status"] == "1")
            {
                Label1.Visible = true;
                Label1.Text = GlobalVariable.NameUser;
            }
            else if (Request.QueryString["status"] != "1")
            {
                Response.Redirect("Login.aspx");
            }
        }

        protected void resendemail_Click(object sender, EventArgs e)
        {
            sendemail();
        }

        public void sendemail()
        {
            string token = getToken();
            string name = GlobalVariable.NameUser;
            if (token != "Failed")
            {
                //string contents = "<html><head runat=\"server\"><title>Activision Account</title><meta charset=\"utf-8\"/><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\"/></head><body><div style=\"width:60%; margin: 0 auto; font-weight:none;\"><br /><br /><div style=\"padding-left:10%; font-size:19px\">Hello " + name + ",<br /><br /><p>Welcome to Blockchain based Certificate Management!</p><p>To activate your account please click the button below to verify your email address:</p></div><br /><center><a style=\"background-color:#6495ED; color:white; padding:2% 4%;\" href=http://localhost:50252/Aktivasi.aspx?token=" + token + ">Activate Account</a><br/><p>This link will expire in 24 hours. Please use this link soon.</p><br/><div style=\"font-size:11px; padding-top:1%;\"></center><br /><div style=\"padding-left:10%; font-size:15px\"></div><br /><br /><div style=\"padding-left:10%; font-size:15px\">Thank you,<br /><br /><br />Blockchain based Certificate Management Team</div></div></body></html>";

                string contents = "<html><head runat=\"server\"><title>Activision Account</title><meta charset=\"utf-8\"/><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\"/></head><body><div style=\"width:60%; margin: 0 auto; font-weight:none;\"><br /><br /><div style=\"padding-left:10%; font-size:19px\">Hello " + name + ",<br /><br /><p>Welcome to Blockchain based Certificate Management!</p><p>To activate your account please click the button below to verify your email address:</p></div><br /><center><a style=\"background-color:#6495ED; color:white; padding:2% 4%;\" href=https://banksertifikat.com/Aktivasi.aspx?token=" + token + ">Activate Account</a><br/><p>This link will expire in 24 hours. Please use this link soon.</p><br/><div style=\"font-size:11px; padding-top:1%;\"></center><br /><div style=\"padding-left:10%; font-size:15px\"></div><br /><br /><div style=\"padding-left:10%; font-size:15px\">Thank you,<br /><br /><br />Blockchain based Certificate Management Team</div></div></body></html>";
                try
                {
                    using (var client = new HttpClient())
                    {
                        Email mailaddres = new Email
                        {
                            mailTo = GlobalVariable.email,
                            mailSubject = "Activation Email",
                            mailContent = contents
                        };

                        client.BaseAddress = new Uri("http://localhost:9999/mail/");
                        var response = client.PostAsJsonAsync("sendMail", mailaddres).Result;
                        var content = response.Content.ReadAsStringAsync().Result;

                        if (response.IsSuccessStatusCode)
                        {
                            Label2.Visible = true;
                            Label2.Text = "Success to send email.";
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                        }
                        else
                        {
                            Label2.Visible = true;
                            Label2.Text = "Failed to send email.";
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Label2.Visible = true;
                    Label2.Text = ex.Message;
                }
            }
        }

        public string getToken()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Users email = new Users
                    {
                        email = GlobalVariable.email
                    };

                    client.BaseAddress = new Uri("http://localhost:9999/users/");
                    var response = client.PostAsJsonAsync("getActivationToken", email).Result;
                    var content = response.Content.ReadAsStringAsync().Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string token = content;
                        return token;
                    }
                    else
                    {
                        return "Failed";
                    }
                }
            }
            catch (Exception ex)
            {
                Label2.Visible = true;
                Label2.Text = ex.Message;
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                return "Failed";
            }
        }
    }
}