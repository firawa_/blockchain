﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage2.Master" AutoEventWireup="true" CodeBehind="Add2.aspx.cs" Inherits="Blockchain_New.Add2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Add Page</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <link href="Add2style.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"/>
    <script src="https://kit.fontawesome.com/16b5cf1a92.js" crossorigin:"anonymous"></script>
    <script type="text/javascript">
        function HideLabel()
        {
            var seconds = 5;
            setTimeout
                (function () {
                    document.getElementById("<%=Labelsukses.ClientID %>").style.display = "none";
                }, seconds * 1000);
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<h2 style="text-align:center"><b>Registration Form</b></h2>
    <br/>
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
                E-mail <span>*</span>
                <asp:TextBox runat="server" CssClass="form-control" ID="TextBoxEmail"></asp:TextBox>
            </div>
            <div class="form-group">
                Gelar Depan <span></span>
                <asp:TextBox runat="server" CssClass="form-control" ID="TextBoxGelardepan"></asp:TextBox>
            </div>
            <div class="form-group">
                Nama Lengkap <span>*</span>
                <asp:TextBox runat="server" CssClass="form-control" ID="TextBoxNamalengkap" Style="text-transform:uppercase" required></asp:TextBox>
            </div>
            <div class="form-group">
                Gelar Belakang <span></span>
                <asp:TextBox runat="server" CssClass="form-control" ID="TextBoxGelarbelakang"></asp:TextBox>
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
                Tempat lahir <span>*</span>
                <div class="col">
                    <asp:DropDownList Style="text-transform:uppercase; width:49%; height:34px; border-color:#ccc !important; font-size:11px" ID="DropDownListProvinsi" runat="server" AutoPostBack ="true" AppendDataBoundItems="true" required OnSelectedIndexChanged="DropDownListProvinsi_SelectedIndexChanged">
                        <asp:ListItem Value="">-- Pilih Provinsi--</asp:ListItem> 
                    </asp:DropDownList>
                    <asp:TextBox ID="TextBoxProvinsi" runat="server" Style="text-transform:uppercase; width:99%; border-color:#ccc !important; font-size:11px" Visible ="false"></asp:TextBox>
                    <asp:DropDownList Style="text-transform:uppercase; width:49%; height:34px; border-color:#ccc !important; font-size:11px" ID="DropDownListKabkota" runat="server" AutoPostBack ="true" AppendDataBoundItems="true" required OnSelectedIndexChanged="DropDownListKabkota_SelectedIndexChanged">
                        <asp:ListItem Value="">-- Pilih Kab/Kota--</asp:ListItem> 
                    </asp:DropDownList>
                    <asp:TextBox ID="TextBoxKotaKab" runat="server" Style="text-transform:uppercase; width:99%; border-color:#ccc !important; font-size:11px" Visible ="false"></asp:TextBox>
                </div>
            </div>
			<div class="form-group">
				Tanggal Lahir <span>*</span>
				<asp:TextBox runat="server" TextMode="Date" CssClass="form-control" ID="TextBoxTanggallahir" required></asp:TextBox>
			</div>
            <div class="form-group">
                Instansi <span>*</span>
                <div class="col">
                    <asp:TextBox ID="TextBoxInstansi" runat="server" CssClass="form-control" Style="text-transform:uppercase;" Enabled="false"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                Nomor Ijazah <span>*</span>
                <asp:TextBox runat="server" CssClass="form-control" ID="TextBoxNomorijazah" required></asp:TextBox>
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
                Tanggal Dikeluarkan Ijazah <span>*</span>
                <asp:TextBox runat="server" TextMode="Date" CssClass="form-control" ID="TextBoxTanggaldikeluarkanijazah" required></asp:TextBox>
            </div>
            <div class="form-group">
                Tahun Terbit Ijazah <span>*</span>
                <asp:TextBox runat="server" CssClass="form-control" ID="TextBoxTahunterbitijazah"  required></asp:TextBox>
            </div>
            <div class="form-group">
                Upload Ijazah <span>*</span>
                <div class="col" >
                    <asp:FileUpload ID="FileUploadIjazah" CssClass="myupload" runat="server" required />
                </div>
            </div>
            <div class="form-group">
                <asp:Button ID="Button1" CssClass="mybutton" runat="server" Text="Submit" OnClick="ButtonSubmit_Click"/>
                <asp:Label ID="Labelsukses" runat="server" CssClass="mylabel"></asp:Label>
            </div>
          </div>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
        </div>
      </div>
</asp:Content>
