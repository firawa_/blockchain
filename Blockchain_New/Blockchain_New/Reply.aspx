﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterpageCoba.Master" AutoEventWireup="true" CodeBehind="Reply.aspx.cs" Inherits="Blockchain_New.Reply" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Reply</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <link href="profilstyle.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link rel="Icon" href="Aset/favicon.png" type="image/png" /> 
    <script src="https://kit.fontawesome.com/16b5cf1a92.js" crossorigin:"anonymous"></script>
    <script type="text/javascript">
        function HideLabel()
        {
            var seconds = 5;
            setTimeout
                (function () {
                    document.getElementById("<%=Label1.ClientID %>").style.display = "none";
                }, seconds * 1000);
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
      <div class="container">
        <div class="row">
          <div class="col-md-2">
          </div>

          <div class="col-md-8">
            <div class="form-group row" style="margin-bottom:0px !important;">
              <label class="col-2 col-form-label">To</label>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <asp:TextBox runat="server" ID="TextBoxEmail" style="width:60% !important;" required></asp:TextBox>
            </div>
            <div class="form-group row">
              <label class="col-2 col-form-label">Subject</label>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <asp:TextBox runat="server" ID="TextBoxSubject" style="width:60% !important;" required></asp:TextBox>
            </div>

              <label style="width: 100px; display: inline-block;" for="TextareaComplain">Complain</label>
              &nbsp;&nbsp;
              <textarea runat="server" style="vertical-align: middle; width:60% !important;" id="TextareaComplain" rows="5" cols="30" required></textarea>
              <br /><br />

              <label style="width: 100px; display: inline-block;" for="Textarea">Answer</label>
              &nbsp;&nbsp;
              <textarea runat="server" style="vertical-align: middle; width:60% !important; height:100px" id="Textarea" rows="1" cols="20" name="S1" required></textarea>

            <div class="form-group">
              <label class="col-2 col-form-label">Attachment</label>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <asp:FileUpload style="width:60%!important; display:unset" ID="FileUploadLampiran" runat="server" />
              <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <asp:Button class="send" ID="Button2" runat="server" Text="Send" OnClick="Button2_Click"/>
              &nbsp;<asp:Label ID="Label1" runat="server" Visible ="false"></asp:Label>
            </div>
          <br />
          <br />
          <br />  
          <br />
          <br />
          <br />
          </div>

          <br />
          <br />  
          <br />
          <br />
          <br />
          <br />

          <div class="col-md-2">
          </div>
        </div>
      </div>
</asp:Content>
