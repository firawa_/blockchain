﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;
using Blockchain_New.Models;
using System.Net.Http.Formatting;
using System.Collections.Generic;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Web.UI.WebControls;

namespace Blockchain_New
{
    public partial class Universitas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["email"] == null && Session["role"] == null)
            {
                logout();
            }
            else if (Session["role"].ToString() == "USER")
            {
                Response.Redirect("Home.aspx");
            }
            else if (Session["role"].ToString() == "SUPERADMIN")
            {
                Response.Redirect("Home3.aspx");
            }
            else if (Session["role"].ToString() == "INSTANSI")
            {
                Response.Redirect("Home4.aspx");
            }
            else if (Session["role"].ToString() == "PERUSAHAAN")
            {
                Response.Redirect("Homeperusahaan.aspx");
            }
            else
            {
                loadmygrid();
            }
        }

        public void logout()
        {
            string email = GlobalVariable.email;
            try
            {
                using (var client = new HttpClient())
                {
                    Users logout = new Users
                    {
                        email = email
                    };

                    client.BaseAddress = new Uri("http://localhost:9999/users/");
                    var response = client.PostAsJsonAsync("logout", logout).Result;
                    var content = response.Content.ReadAsStringAsync().Result;

                    if (response.IsSuccessStatusCode)
                    {
                        Session.Abandon();
                        Response.Redirect("Login.aspx");
                    }

                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            addUniversity();
            loadmygrid();
        }

        public void loadmygrid()
        {
            string email = Session["email"].ToString();

            if (email != null)
            {
                try
                {
                    HttpClient clients = new HttpClient();
                    clients.BaseAddress = new Uri("http://localhost:9999/instansi/");
                    clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage rs = clients.GetAsync("daftarInstansi").Result;
                    if (rs.IsSuccessStatusCode)
                    {
                        var content = rs.Content.ReadAsStringAsync().Result;
                        Data.DataSource = (new JavaScriptSerializer()).Deserialize<List<Instansi>>(content);
                        Data.DataBind();
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }

        public void addUniversity()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Instansi university = new Instansi
                    {
                        instansiName = TextBoxNamauniversitas.Text.Trim().ToUpper()
                    };

                    client.BaseAddress = new Uri("http://localhost:9999/instansi/");
                    var response = client.PostAsJsonAsync("tambahInstansi", university).Result;
                    var content = response.Content.ReadAsStringAsync().Result;

                    if (response.IsSuccessStatusCode)
                    {
                        TextBoxNamauniversitas.Text = string.Empty;

                        Labelberhasil.Visible = true;
                        Labelberhasil.ForeColor = System.Drawing.Color.Green;
                        Labelberhasil.Text = content;
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else
                    {
                        Labelberhasil.Visible = true;
                        Labelberhasil.ForeColor = System.Drawing.Color.Red;
                        Labelberhasil.Text = content;
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                }

            }
            catch (Exception ex)
            {
                Labelberhasil.Visible = true;
                Labelberhasil.ForeColor = System.Drawing.Color.Red;
                Labelberhasil.Text = ex.Message;
            }
        }

        protected void Delete_Click(object Sender, EventArgs e)
        {
            LinkButton detailbutton = Sender as LinkButton;

            GridViewRow record = detailbutton.NamingContainer as GridViewRow;

            string university_name = record.Cells[0].Text;

            try
            {
                using (var client = new HttpClient())
                {
                    Instansi university = new Instansi
                    {
                        instansiName = university_name
                    };

                    client.BaseAddress = new Uri("http://localhost:9999/instansi/");
                    var response = client.PostAsJsonAsync("hapusInstansi", university).Result;
                    var content = response.Content.ReadAsStringAsync().Result;

                    if (response.IsSuccessStatusCode)
                    {
                        loadmygrid();
                        Labelberhasil.Visible = true;
                        Labelberhasil.ForeColor = System.Drawing.Color.ForestGreen;
                        Labelberhasil.Text = "Success to delete instansi.";
                    }
                }
            }
            catch (Exception ex)
            {
                
            }
        }
    }
}