﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Registerperusahaan.aspx.cs" Inherits="Blockchain_New.Registerperusahaan" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Register Perusahaan</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="Registerstyle.css" rel="stylesheet" />
    <link rel="Icon" href="Aset/favicon.png" type="image/png" /> 
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"/>
    <script type="text/javascript">
        function HideLabel()
        {
            var seconds = 5;
            setTimeout
                (function () {
                    document.getElementById("<%=Label1Register.ClientID %>").style.display = "none";
                }, seconds * 1000);
        };
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <nav class="navbar navbar-default">
      <div class="container" style="width: 98%!important">
        <div class="navbar-header"> 
<div style="align: justify; color:white; width:300px">
<img height="100" src="Aset/Logo.jpeg" style="float: left; margin-right:9px; height:60px" width="80" /><p style="padding-top:6.8%; margin-bottom:0px!important">Blockchain based <br />Certificate Management</p></div>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="FAQ.aspx">FAQ</a></li>
            <li><a href="ContactUs.aspx">Contact Us</a></li>
            <li><a href="Login.aspx">Login/Register</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
<br />

      <div class="containerr">
        <div class="row">
          <div class="col-md-8">
          </div>

          <div class="col-md-4">
            <center style="color:white; font-size:19px;">Register Perusahaan</center>
            <div class="panel panel-default">
            </div>

            <div class="panel panel-default">
              <div class="panel-body">
                  <div class="back">
                      <div class="dalam" style="margin-top:0px !important">
                      <br />
                        <center><img class="card-img-top" src="Aset/orang.png" alt="Card image cap"/></center>
                        <br />
                <center>
                    <a class="btn btn-primary" href="Register.aspx" role="button" style="background-color:white; color:black;">Individu</a>
                    <a class="btn btn-primary" href="Register2.aspx" role="button" style="background-color:white; color:black;">Instansi</a>
                    <a class="btn btn-primary" role="button">Perusahaan</a>
                </center>
                        <div class="row">
                          <div class="col-25">
                            <label for="Instansi">Nama&nbsp;Perusahaan</label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-75" style="margin-top:0 !important">
                            <asp:TextBox Style="text-transform:uppercase;" CssClass="form-control" ID="TextBoxPerusahaan" runat="server" required></asp:TextBox>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-25">
                            <label for="Username">E-mail</label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-75" style="margin-top:0 !important">
                            <asp:TextBox CssClass="form-control" ID="TextBoxUsername" runat="server" MaxLength ="40" required></asp:TextBox>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-25">
                            <label for="Password">Password</label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-75" style="margin-top:0 !important; margin-bottom:0 !important">
                            <asp:TextBox CssClass="form-control" ID="TextBoxPassword" runat="server" TextMode="Password" required></asp:TextBox>
                          </div>
                        </div>
                                <p>
                                    <asp:Label ID="Label1Register" runat="server"></asp:Label>
                                </p>

                        <div class="row">
							<asp:Button CssClass="btn btn-primary" style="margin-left:5%" ID="ButtonRegister" runat="server" Text="Register" OnClick="ButtonRegister_Click"/>
							<a href="Login.aspx" style="margin-left:32%; font-size:14px!important" class="card-link">Punya akun?Login</a>
                        </div>
                        <br />
                      </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    </form>
</body>
</html>
