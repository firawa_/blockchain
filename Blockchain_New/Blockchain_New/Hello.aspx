﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Hello.aspx.cs" Inherits="Blockchain_New.Hello" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Hello</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <link href="Accountsettingstyle.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link rel="Icon" href="Aset/favicon.png" type="image/png" /> 
    <script src="https://kit.fontawesome.com/16b5cf1a92.js" crossorigin:"anonymous"></script>
</head>
<body>
    <form id="form1" runat="server">
        <header id="header" style="background-color: #0d1463; color:#ffffff; padding-bottom: 10px; padding-left:5px;">
            <div class="row">
              <div class="col-md-10" style="font-size:40px !important; padding-top:18px; padding-bottom:10px;">
<div style="align: justify; color:white; width:100%; font-size:23px">
<img height="100" src="Aset/Logo.jpeg" style="float: left; margin-right:9px; height:60px" width="80" />Blockchain based <br />Certificate Management</div>
              </div>
            </div>
        </header>
      <div class="container">
        <div class="row">
          <div style="text-align:center; font-size:18px" class="col-md-12">
<br /><br /><br /><br />
              Hello
              <p>Someone hopefully you has requested to reset the password for your account on <a>http://banksertifikat.com</a></p>
              <p>If you did not perform this request, you can safely ignore this email</p>
              <p>Otherwise, click the link below to complete the process.</p>
              <a href="ResetPassword.aspx">Reset Password</a>
          </div>
        </div>
      </div>
   </form>
</body>
</html>
