﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;
using Blockchain_New.Models;
using System.Net.Http.Formatting;
using System.Collections.Generic;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Web.UI.WebControls;

namespace Blockchain_New
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["status"] == "2")
            {
                Label1Login.Visible = true;
                Label1Login.ForeColor = System.Drawing.Color.Green;
                Label1Login.Text = "Your Password has been succesfully to change.";
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
            }
            else if (Request.QueryString["status"] == "3")
            {
                Label1Login.Visible = true;
                Label1Login.ForeColor = System.Drawing.Color.Green;
                Label1Login.Text = "Your account has been succesfully to activate.";
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
            }

            if (Session["email"] != null)
            {
                if (Session["role"].ToString().Equals("ADMIN"))
                {
                    Response.Redirect("Home2.aspx");
                }
                else if (Session["role"].ToString().Equals("USER"))
                {
                    Response.Redirect("Home.aspx");
                }
                else if (Session["role"].ToString().Equals("SUPERADMIN"))
                {
                    Response.Redirect("Home3.aspx");
                }
                else if (Session["role"].ToString().Equals("INSTANSI"))
                {
                    Response.Redirect("Home4.aspx");
                }
                else if (Session["role"].ToString().Equals("PERUSAHAAN"))
                {
                    Response.Redirect("Homeperusahaan.aspx");
                }
            }
        }

        protected void ButtonLogin_Click(object sender, EventArgs e)
        {
            GlobalVariable.email = TextBoxUsername.Text;
            GetLogin();
        }


        public void GetLogin()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Users user = new Users
                    {
                        email = TextBoxUsername.Text.Trim(),
                        password = TextBoxPassword.Text
                    };

                    client.BaseAddress = new Uri("http://localhost:9999/users/");
                    var response = client.PostAsJsonAsync("login", user).Result;

                    if (response.StatusCode.ToString() == "OK")
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        var userlist = JsonConvert.DeserializeObject<Users>(content);

                        if (userlist.activated == true)
                        {
                            Session["email"] = userlist.email;
                            GlobalVariable.email = userlist.email;
                            GlobalVariable.name = userlist.name;
                            string role = userlist.roleName;
                            Session["role"] = role;
                            if (role == "ADMIN")
                            {
                                Response.Redirect("Home2.aspx");
                            }
                            else if (role == "SUPERADMIN")
                            {
                                Response.Redirect("Home3.aspx");
                            }
                            else if (role == "INSTANSI")
                            {
                                Response.Redirect("Home4.aspx");
                            }
                            else if (role == "PERUSAHAAN")
                            {
                                Response.Redirect("Homeperusahaan.aspx");
                            }
                            else
                            {
                                Response.Redirect("Home.aspx");
                            }
                        }
                        else if (userlist.activated == false)
                        {
                            GlobalVariable.NameUser = getName();
                            if (GlobalVariable.NameUser != "Failed404")
                            {
                                Response.Redirect("Himbauan.aspx?status=1");
                            }    
                        }
                    }
                    else if (response.StatusCode.ToString() == "BadRequest")
                    {
                        Label1Login.Visible = true;
                        Label1Login.ForeColor = System.Drawing.Color.Red;
                        Label1Login.Text = "Your account is currently logged in. Please log in again.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else
                    {
                        Label1Login.Visible = true;
                        Label1Login.ForeColor = System.Drawing.Color.Red;
                        Label1Login.Text = "Invalid an email or a password. Please try again!";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                Label1Login.Text = ex.Message;
            }
        }

        public string getName()
        {
            try
            {
                string email = TextBoxUsername.Text;
                string uri = "http://localhost:9999/";
                string parameter = "users/cariUsers?email=" + email + "";
                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri(uri);
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = clients.GetAsync(parameter).Result;

                if (response.IsSuccessStatusCode)
                {
                    var content = response.Content.ReadAsStringAsync().Result;
                    var userdata = JsonConvert.DeserializeObject<Users>(content);

                    string name = userdata.name;
                    return name;
                }
                else
                {
                    return "Failed404";
                }
            }
            catch (Exception ex)
            {
                Label1Login.Visible = true;
                Label1Login.Text = ex.Message;
                return "Failed404";
            }
        }
    }
}