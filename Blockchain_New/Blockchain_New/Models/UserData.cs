﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blockchain_New.Models
{
    public class UserData
    {
        public string gelarBelakang { get; set; }
        public string gelarDepan { get; set; }
        public int idData { get; set; }
        public string nama { get; set; }
        public string noIjazah { get; set; }
        public string scanFile { get; set; }
        public DateTime tglIjazah { get; set; }
        public int thnIjazah { get; set; }
        public string instansi { get; set; }
        public string email { get; set; }
        public string ttl { get; set; }
        public string picEmail { get; set; }
    }
}