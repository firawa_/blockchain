﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blockchain_New.Models
{
    public class Blockchain
    {
        public int idData { get; set; }
        public int indexBlock { get; set; }
        public int nonce { get; set; }
        public string previousHash { get; set; }
        public DateTime timestamp { get; set; }
        public string userName { get; set; }
    }
}