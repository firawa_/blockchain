﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blockchain_New.Models
{
    public class Email
    {
        public string mailFrom { get; set; }
        public string mailTo { get; set; }
        public string mailCc { get; set; }
        public string mailBcc { get; set; }
        public string mailSubject { get; set; }
        public string mailContent { get; set; }
        public string contentType { get; set; }
        public string attachment { get; set; }
    }
}