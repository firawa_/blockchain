﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blockchain_New.Models
{
    public class Certifikat
    {
        public string email;
        public string name;
        public float score;
        public int semester;
        public int sks;
        public DateTime test_date;
        public string test_name;
    }
}