﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blockchain_New.Models
{
    public class Laporan
    {
        public int idLaporan { get; set; }

        public string namaDepan { get; set; }

        public string namaBelakang { get; set; }

        public string email { get; set; }

        public DateTime tanggalLaporan { get; set; }

        public string judulLaporan { get; set; }

        public string isiLaporan { get; set; }

        public string lampiranLaporan { get; set; }

        public string statusLaporan { get; set; }
    }
}