﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blockchain_New.Models
{
    public class Users
    {
        public string email { get; set; }
        public string password { get; set; }
        public string roleName { get; set; }
        public int channel { get; set; }
        public DateTime registeredDate { get; set; }
        public DateTime registeredTime { get; set; }
        public bool isActive { get; set; }
        public string name { get; set; }
        public bool activated { get; set; }
    }
}