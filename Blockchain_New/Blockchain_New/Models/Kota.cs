﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blockchain_New.Models
{
    public class Kota
    {
        public int cityId { get; set; }
        public string cityName { get; set; }
        public int provinceId { get; set; }
    }
}