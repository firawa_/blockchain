﻿using System;
using System.Net;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using Blockchain_New.Models;
using System.Net.Http.Formatting;
using System.Collections.Generic;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text.RegularExpressions;


namespace Blockchain_New
{
    public partial class ResetPassword : System.Web.UI.Page
    {
        public string token { get; set; }
        public string emailGet { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            token = Request.QueryString["token"];
            emailGet = GetEmail();
            if (emailGet == "Token invalid")
            {
                Response.Redirect("ForgetPassword.aspx?status=1");
            }
        }

        public string GetEmail()
        {
            try
            {
                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri("http://localhost:9999/users/");
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = clients.GetAsync("cekEmail?token=" + token).Result;

                if (response.IsSuccessStatusCode)
                {
                    var content = response.Content.ReadAsStringAsync().Result;
                    return content;
                }
                else
                {
                    return "Token invalid";
                }
            }
            catch (Exception ex)
            {
                LabelBerhasil.Visible = true;
                LabelBerhasil.Text = ex.Message;
                return "Email not found";
            }
        }
        public void resetpassword()
        {
            try
            {
                using (var client2 = new HttpClient())
                {
                    Users account = new Users
                    {
                        email = emailGet,
                        password = TextBoxNew.Text
                    };

                    var hasNumber = new Regex(@"[0-9]+");
                    var hasUpperChar = new Regex(@"[A-Z]+");
                    var hasMiniMaxChars = new Regex(@".{8,32}");
                    var hasLowerChar = new Regex(@"[a-z]+");
                    var hasSymbols = new Regex(@"[!@#$%^&*()_+=\[{\]};:<>|./?,-]");

                    var pass = TextBoxNew.Text.Trim();

                    if (!hasLowerChar.IsMatch(pass))
                    {
                        LabelBerhasil.Visible = true;
                        LabelBerhasil.Text = "Your password should contain at least one lower case letter.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else if (!hasUpperChar.IsMatch(pass))
                    {
                        LabelBerhasil.Visible = true;
                        LabelBerhasil.Text = "Your password should contain at least one upper case letter.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else if (!hasMiniMaxChars.IsMatch(pass))
                    {
                        LabelBerhasil.Visible = true;
                        LabelBerhasil.Text = "Your password should not be less than 8 characters.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else if (!hasNumber.IsMatch(pass))
                    {
                        LabelBerhasil.Visible = true;
                        LabelBerhasil.Text = "Your password should contain at least one numeric value.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else if (!hasSymbols.IsMatch(pass))
                    {
                        LabelBerhasil.Visible = true;
                        LabelBerhasil.Text = "Your password should contain at least one special case characters.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else
                    {
                        if (TextBoxNew.Text.Trim() == TextBoxConfirm.Text.Trim())
                        {
                            client2.BaseAddress = new Uri("http://localhost:9999/users/");
                            var response2 = client2.PostAsJsonAsync("gantiPasswordUser?token=" + token, account).Result;
                            var content2 = response2.Content.ReadAsStringAsync().Result;

                            if (response2.IsSuccessStatusCode)
                            {
                                TextBoxNew.Text = string.Empty;
                                TextBoxConfirm.Text = string.Empty;
                                LabelBerhasil.Visible = true;
                                LabelBerhasil.ForeColor = System.Drawing.Color.Green;
                                LabelBerhasil.Text = "Your password is successfully to change.";
                                ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                                Response.Redirect("Login.aspx?status=2");
                            }
                            else if (response2.StatusCode.ToString() == "BadRequest" )
                            {
                                LabelBerhasil.Visible = true;
                                LabelBerhasil.Text = "Your new password must differ from the old password.";
                                ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                            }
                            else
                            {
                                Response.Redirect("ForgetPassword.aspx?status=1");
                            }
                        }
                        else
                        {
                            LabelBerhasil.Visible = true;
                            LabelBerhasil.Text = "Your new password and confirm password is not the same.";
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LabelBerhasil.Visible = true;
                LabelBerhasil.Text = ex.Message;
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
            }
        }

        protected void ButtonSearch_Click(object sender, EventArgs e)
        {
            resetpassword();
        }
    }
}