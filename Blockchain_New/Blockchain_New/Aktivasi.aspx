﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Aktivasi.aspx.cs" Inherits="Blockchain_New.Aktivasi" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Activision Account</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <link href="Accountsettingstyle.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link rel="Icon" href="Aset/favicon.png" type="image/png" /> 
    <script src="https://kit.fontawesome.com/16b5cf1a92.js" crossorigin:"anonymous"></script>
</head>
<body>
    <form id="form1" runat="server">
        <header id="header" style="background-color: #0d1463; color:#ffffff; padding-bottom: 10px; padding-left:5px;">
            <div class="row">
              <div class="col-md-10" style="font-size:40px !important; padding-top:18px; padding-bottom:10px;">
<div style="align: justify; color:white; width:100%; font-size:23px">
<a href="Login.aspx"><img height="100" src="Aset/Logo.jpeg" style="float: left; margin-right:9px; height:60px" width="80"/></a>Blockchain based <br />Certificate Management</div>
              </div>
            </div>
        </header>
      <div class="container">
        <div class="row">
          <div class="col-md-2">

          </div>

          <div class="col-md-8">
            <br /><br /><br /><br />
            <h4 style="padding-left:10%">
                Hello,
                <br /><br />
                <p>Welcome to Blockchain based Certificate Management!</p>
                <p>To activate your account please click the button below to verify your email address:</p>
            </h4>
            <br />
            <div class="row">
                <center>
                    <asp:Button style="background-color:#3c90c8; color:white; padding:2% 4%; border:none" ID="ButtonSearch" runat="server" Text="Activate Account" OnClick="ButtonSearch_Click"/>
                </center>
                <br />
                <h5 style="padding-left:10%">Thank you,
                <br /><br /><br /><br />Blockchain based Certificate Management Team</h5>
            </div>
            <br />
          </div>
        </div>

          <div class="col-md-2">

          </div>
        </div>
    </form>
</body>
</html>
