﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterpageCoba.Master" AutoEventWireup="true" CodeBehind="DetailLaporan.aspx.cs" Inherits="Blockchain_New.DetailLaporan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Detail Laporan</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <link href="Detailstyle.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link rel="Icon" href="Aset/favicon.png" type="image/png" /> 
    <script src="https://kit.fontawesome.com/16b5cf1a92.js" crossorigin:"anonymous"></script>
    <script type="text/javascript">
        function DisplayImageInNewWindow()
        {
            var Image = document.getElementById('<%= Lampiran.ClientID %>').src;

            html = "<HTML><HEAD><TITLE>Photo</TITLE>"
                + "</HEAD><BODY LEFTMARGIN=0 "
                + "MARGINWIDTH=0 TOPMARGIN=0 MARGINHEIGHT=0><CENTER>"
                + "<IMG src='"
                + Image
                + "' BORDER=0 NAME=image "
                + "onload='window.resizeTo(document.image.width,document.image.height)'>"
                + "</CENTER>"
                + "</BODY></HTML>";
            popup = window.open('', 'image', 'toolbar=0,location=0,directories=0,menuBar=0,scrollbars=0,resizable=1');
            popup.document.open();
            popup.document.write(html);
            popup.document.focus();
            popup.document.close();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<h2 style="text-align:center"><b>Report Detail</b></h2>
    <br/>
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
                Id
                <asp:TextBox runat="server" value="" CssClass="form-control" ID="TextBoxIDLaporan" ReadOnly="true"></asp:TextBox>
            </div>
            <div class="form-group">
                First Name
                <asp:TextBox runat="server" value="" CssClass="form-control" ID="TextBoxNamadepan" ReadOnly="true"></asp:TextBox>
            </div>
            <div class="form-group">
                Last Name
                <asp:TextBox runat="server" value="" CssClass="form-control" ID="TextBoxNamabelakang" ReadOnly="true"></asp:TextBox>
            </div>
            <div class="form-group">
                E-mail
                <asp:TextBox runat="server" value="" CssClass="form-control" ID="TextBoxEmail" ReadOnly="true"></asp:TextBox>
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
                Date
                <asp:TextBox runat="server" value="" CssClass="form-control" ID="TextBoxTanggallaporan" ReadOnly="true"></asp:TextBox>
            </div>
            <div class="form-group">
                Subject
                <asp:TextBox runat="server" value="" CssClass="form-control" ID="TextBoxSubjeklaporan" ReadOnly="true"></asp:TextBox>
            </div>
            <div class="form-group">
                Content
                <textarea id="TextareaIsi" runat="server" value="" class="form-control" style="height:115px !important;" ReadOnly="true"></textarea>
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
                Attachment
                <p>
                    <asp:ImageButton ID ="Lampiran" runat="server" ReadOnly="true" OnClientClick = "DisplayImageInNewWindow(); return false" runat="server"></asp:ImageButton>
                </p>
                <div class="below-attachment">
                    <asp:Label ID="Labelsukses" runat="server" CssClass="mylabel" Text="test" Visible="false"></asp:Label>
                    <asp:Button ID="Button1" CssClass="mybutton" runat="server" Text="Open PDF" Visible="false" OnClick="Button1_Click"/>
                </div>
            </div>
            <div class="form-group">
                <div class="below-attachment">
                    <p style="font-size:15px;">Status: </p>
                    <asp:Label ID="LabelStatus" runat="server" style="font-weight:bold; font-size:20px; color:red;"></asp:Label>
                    <asp:Button ID="Button3" CssClass="mybutton1" runat="server" Text="Reply" OnClick="Button3_Click"/>
                </div>
            </div>  
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />         
          </div>
        </div>
      </div>
</asp:Content>
