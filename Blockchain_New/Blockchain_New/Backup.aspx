﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterpageCoba.Master" AutoEventWireup="true" CodeBehind="Backup.aspx.cs" Inherits="Blockchain_New.Backup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Backup Page</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <link href="Universitasstyle.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link rel="Icon" href="Aset/favicon.png" type="image/png" /> 
    <script src="https://kit.fontawesome.com/16b5cf1a92.js" crossorigin:"anonymous"></script>
    <script type="text/javascript">
        function HideLabel()
        {
            var seconds = 5;
            setTimeout
                (function () {
                    document.getElementById("<%=Labelberhasil.ClientID %>").style.display = "none";
                }, seconds * 1000);
        };
    </script>
    <script type="text/javascript">
        function DisplayImageInNewWindow() {
            var Image = document.getElementById('<%= Certif.ClientID %>').src;

            html = "<HTML><HEAD><TITLE>Photo</TITLE>"
                + "</HEAD><BODY LEFTMARGIN=0 "
                + "MARGINWIDTH=0 TOPMARGIN=0 MARGINHEIGHT=0><CENTER>"
                + "<IMG src='"
                + Image
                + "' BORDER=0 NAME=image "
                + "onload='window.resizeTo(document.image.width,document.image.height)'>"
                + "</CENTER>"
                + "</BODY></HTML>";
            popup = window.open('', 'image', 'toolbar=0,location=0,directories=0,menuBar=0,scrollbars=0,resizable=1');
            popup.document.open();
            popup.document.write(html);
            popup.document.focus();
            popup.document.close();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
                        <asp:Button ID="Button" CssClass="myadd2" style="margin-left:5%" runat="server" Text="Backup Data" OnClick="Button1_Click"/>
                        <asp:Label ID="Labelberhasil" runat="server" CssClass="label" style="color:green; font-size:12px"></asp:Label>
    <br />
    <br />
    <asp:GridView ID="Data" CssClass="mydata" runat="server" AutoGenerateColumns="False" style="margin:0 auto; width:90%">
					<Columns>
						<asp:BoundField DataField="name" HeaderText="Name" />
						<asp:BoundField DataField="email" HeaderText="E-mail" />
                        <asp:BoundField DataField="test_name" HeaderText="Test Name" />
                        <asp:BoundField DataField="score" HeaderText="Score" />
						<asp:BoundField DataField="test_date" HtmlEncode="false" DataFormatString="{0:yyyy/MM/dd}" HeaderText="Test Date" />
						<asp:BoundField DataField="semester" HeaderText="Semester" />
						<asp:BoundField DataField="sks" HeaderText="SKS" />
                        <asp:TemplateField>
							<ItemTemplate>
							   <asp:LinkButton runat="server" CssClass="mydetail2" ID="Generate" Text="Get Certificate" OnClick = "Generate_Click" ></asp:LinkButton>
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
    </asp:GridView>
    <br />
    <center><asp:ImageButton ID ="Certif" runat="server" Visible ="false" OnClientClick = "DisplayImageInNewWindow(); return false" ReadOnly="true"></asp:ImageButton></center>

<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
</asp:Content>
