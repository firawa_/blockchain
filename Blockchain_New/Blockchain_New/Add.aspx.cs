﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;
using Blockchain_New.Models;
using System.Net.Http.Formatting;
using System.Collections.Generic;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Web.UI.WebControls;
using System.Runtime.CompilerServices;
using System.Linq;

namespace Blockchain_New
{
    public partial class Add : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["email"] == null && Session["role"] == null)
            {
                logout();
            }
            else if (Session["role"].ToString() == "ADMIN")
            {
                Response.Redirect("Home2.aspx");
            }
            else if (Session["role"].ToString() == "SUPERADMIN")
            {
                Response.Redirect("Home3.aspx");
            }
            else if (Session["role"].ToString() == "INSTANSI")
            {
                Response.Redirect("Home4.aspx");
            }
            else if (Session["role"].ToString() == "PERUSAHAAN")
            {
                Response.Redirect("Homeperusahaaan.aspx");
            }
            else
            {
                if (!Page.IsPostBack)
                {
                    LoadMyDropDownProv();
                    LoadMyDropDownUniv();
                }
            }
        }

        public void logout()
        {
            string email = GlobalVariable.email;
            try
            {
                using (var client = new HttpClient())
                {
                    Users logout = new Users
                    {
                        email = email
                    };

                    client.BaseAddress = new Uri("http://localhost:9999/users/");
                    var response = client.PostAsJsonAsync("logout", logout).Result;
                    var content = response.Content.ReadAsStringAsync().Result;

                    if (response.IsSuccessStatusCode)
                    {
                        Session.Abandon();
                        Response.Redirect("Login.aspx");
                    }

                }
            }
            catch (Exception ex)
            {

            }
        }

        public void LoadMyDropDownProv()
        {
            string email = Session["email"].ToString();

            if (email != null)
            {
                try
                {
                    HttpClient clients = new HttpClient();
                    clients.BaseAddress = new Uri("http://localhost:9999/provinsi/");
                    clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage response = clients.GetAsync("daftarProvinsi").Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        List<Provinsi> provinsiList = (new JavaScriptSerializer()).Deserialize<List<Provinsi>>(content);
                        DropDownListProvinsi.DataSource = provinsiList;
                        DropDownListProvinsi.DataTextField = "provinceName";
                        DropDownListProvinsi.DataValueField = "provinceId";
                        DropDownListProvinsi.DataBind();
                        DropDownListProvinsi.Items.Add("OTHER");
                    }

                }
                catch (Exception ex)
                {

                }
            }
        }

        public void LoadMyDropDownKota()
        {
            string email = Session["email"].ToString();

            if (email != null)
            {
                try
                {
                    string id = DropDownListProvinsi.SelectedItem.Value.ToString();
                    string uri = "http://localhost:9999/";
                    string parameter = "kota/daftarKotaByProvince?provinceId=" + id + "";
                    HttpClient clients = new HttpClient();
                    clients.BaseAddress = new Uri(uri);
                    clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage response = clients.GetAsync(parameter).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        List<Kota> kotaList = (new JavaScriptSerializer()).Deserialize<List<Kota>>(content);
                        DropDownListKabkota.DataSource = kotaList;
                        DropDownListKabkota.DataTextField = "cityName";
                        DropDownListKabkota.DataValueField = "cityId";
                        DropDownListKabkota.DataBind();
                        DropDownListKabkota.Items.Add("OTHER");
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }

        public void LoadMyDropDownUniv()
        {
            string email = Session["email"].ToString();

            if (email != null)
            {
                try
                {
                    HttpClient clients = new HttpClient();
                    clients.BaseAddress = new Uri("http://localhost:9999/instansi/");
                    clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage response = clients.GetAsync("daftarInstansi").Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        List<Instansi> universitasList = (new JavaScriptSerializer()).Deserialize<List<Instansi>>(content);
                        DropDownListUniversitas.DataSource = universitasList;
                        DropDownListUniversitas.DataTextField = "instansiName";
                        DropDownListUniversitas.DataValueField = "instansiId";
                        DropDownListUniversitas.DataBind();
                        DropDownListUniversitas.Items.Add("OTHER");
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }



        protected void DropDownListProvinsi_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownListProvinsi.SelectedItem.Text == "OTHER")
            {
                DropDownListKabkota.Items.Clear();
                DropDownListKabkota.Items.Add("OTHER");
                TextBoxKotaKab.Visible = true;
                TextBoxKotaKab.Text = string.Empty;
            }
            else if (DropDownListProvinsi.SelectedItem.Text == "--PILIH PROVINSI--")
            {
                TextBoxProvinsi.Visible = false;
                //TextBoxProvinsi.Text = string.Empty;
                DropDownListKabkota.Items.Clear();
                DropDownListKabkota.Items.Add("--PILIH KAB/KOTA--");
                TextBoxKotaKab.Visible = false;
                //TextBoxKotaKab.Text = string.Empty;
            }
            else
            {
                TextBoxProvinsi.Visible = false;
                //TextBoxProvinsi.Text = string.Empty;
                TextBoxKotaKab.Visible = false;
                //TextBoxKotaKab.Text = string.Empty;
                DropDownListKabkota.Items.Clear();
                DropDownListKabkota.Items.Add("--PILIH KAB/KOTA--");
                LoadMyDropDownKota();
            }
        }

        protected void DropDownListKabkota_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownListKabkota.SelectedItem.Text == "OTHER")
            {
                TextBoxKotaKab.Visible = true;
                TextBoxKotaKab.Text = string.Empty;
            }
            else if (DropDownListKabkota.SelectedItem.Text == "--PILIH KOTA--")
            {
                TextBoxKotaKab.Visible = false;
                TextBoxKotaKab.Text = string.Empty;
            }
            else
            {
                TextBoxKotaKab.Visible = false;
                TextBoxKotaKab.Text = string.Empty;
            }
        }

        public string CheckData()
        {
            if (DropDownListUniversitas.SelectedItem.Text != "OTHER")
            {
                string instansi = DropDownListUniversitas.SelectedItem.Text;
                return instansi;
            }
            else
            {
                string instansi = TextBoxInstansi.Text.ToUpper();
                return instansi;
            }
        }

        public string CheckDataKabKota()
        {
            if (DropDownListProvinsi.SelectedItem.Text != "OTHER")
            {
                string kabkota = DropDownListKabkota.SelectedItem.Text;
                return kabkota;
            }
            else
            {
                string kabkota = TextBoxKotaKab.Text.ToUpper();
                return kabkota;
            }
        }

        public string GetRandomName()
        {
            string extention = System.IO.Path.GetExtension(FileUploadIjazah.FileName);
            if (extention == ".pdf")
            {
                string random_name = Convert.ToString(Guid.NewGuid()) + ".pdf";
                return random_name;
            }
            else
            {
                string random_name = Convert.ToString(Guid.NewGuid()) + ".jpg";
                return random_name;
            }
        }

        UserData userData = new UserData();

        public void InputData()
        {
            string email = Session["email"].ToString();
            string instansi_name = CheckData();
            string kabkota = CheckDataKabKota();
            string random_name = GetRandomName();

            if (email != null)
            {
                try
                {
                    string Image = FileUploadIjazah.FileName.ToString();
                    string extention = System.IO.Path.GetExtension(FileUploadIjazah.FileName);
                    if (extention == ".pdf")
                    {
                        FileUploadIjazah.PostedFile.SaveAs(Server.MapPath("~/Upload/PDF/" + random_name));
                    }
                    else
                    {
                        FileUploadIjazah.PostedFile.SaveAs(Server.MapPath("~/Upload/Image/" + random_name));
                    }         
                    using (var client = new HttpClient())
                    {
                        userData.email = email;
                        userData.gelarBelakang = TextBoxGelarbelakang.Text;
                        userData.gelarDepan = TextBoxGelardepan.Text;
                        userData.nama = TextBoxNamalengkap.Text.ToUpper();
                        userData.ttl = kabkota + ", " + TextBoxTanggallahir.Text;
                        userData.noIjazah = TextBoxNomorijazah.Text.Trim();
                        userData.scanFile = random_name;
                        userData.tglIjazah = DateTime.Parse(TextBoxTanggaldikeluarkanijazah.Text).Date;
                        userData.instansi = instansi_name;
                        userData.thnIjazah = int.Parse(TextBoxTahunterbitijazah.Text.Trim());
                        userData.picEmail = String.Empty;

                        client.BaseAddress = new Uri("http://localhost:9999/userdata/");
                        var response = client.PostAsJsonAsync("tambahUserData", userData).Result;
                        var content = response.Content.ReadAsStringAsync().Result;

                        if (response.IsSuccessStatusCode)
                        {
                            var users = JsonConvert.DeserializeObject<UserData>(content);

                            userData.idData = users.idData;
                        }
                        else
                        {
                            Labelsukses.Visible = true;
                            Labelsukses.Text = "Error Input Data";
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                        }
                    }

                }
                catch (Exception ex)
                {
                    Labelsukses.Visible = true;
                    Labelsukses.Text = ex.Message;
                }
            }
        }

        public void InputDataBlockchain()
        {
            string email = Session["email"].ToString();

            if (email != null)
            {
                try
                {
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri("http://localhost:9999/blockchain/");
                        var response = client.PostAsJsonAsync("tambahBlockchain", userData).Result;
                        var content = response.Content.ReadAsStringAsync().Result;

                        if (response.IsSuccessStatusCode)
                        {
                            TextBoxGelardepan.Text = string.Empty;
                            TextBoxNamalengkap.Text = string.Empty;
                            TextBoxGelarbelakang.Text = string.Empty;
                            TextBoxTanggallahir.Text = string.Empty;
                            TextBoxNomorijazah.Text = string.Empty;
                            TextBoxTahunterbitijazah.Text = string.Empty;
                            TextBoxTanggaldikeluarkanijazah.Text = string.Empty;
                            TextBoxKotaKab.Visible = false;
                            TextBoxKotaKab.Text = string.Empty;
                            TextBoxInstansi.Visible = false;
                            TextBoxInstansi.Text = string.Empty;
                            DropDownListProvinsi.SelectedIndex = 0;
                            DropDownListKabkota.Items.Clear();
                            DropDownListKabkota.Items.Add("--PILIH KAB/KOTA--");
                            DropDownListKabkota.SelectedIndex = 0;
                            DropDownListUniversitas.SelectedIndex = 0;
                            FileUploadIjazah.Attributes.Clear();

                            Labelsukses.Visible = true;
                            Labelsukses.ForeColor = System.Drawing.Color.Green;
                            Labelsukses.Text = "Data added successfully.";
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Labelsukses.Visible = true;
                    Labelsukses.Text = ex.Message;
                }
            }
        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            int number;
            bool isNumeric = int.TryParse(TextBoxTahunterbitijazah.Text, out number);
            string extention_file = System.IO.Path.GetExtension(FileUploadIjazah.FileName);
            string[] list_extention = { ".jpg", ".jpeg", ".png", ".pdf"};
            if (!list_extention.Contains(extention_file))
            {
                Labelsukses.Visible = true;
                Labelsukses.Text = "Your file must an image.";
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
            }
            else if (isNumeric == false)
            {
                Labelsukses.Visible = true;
                Labelsukses.Text = "Year of publication must be an integer number.";
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
            }
            else
            {
                ListItem list = DropDownListUniversitas.Items.FindByText(TextBoxInstansi.Text.ToUpper());
                if (list != null)
                {
                    Labelsukses.Visible = true;
                    Labelsukses.Text = "Instansi Name already exists in Instansi Dropdown. Please choose Instansi Name in Instansi Dropdown.";
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                }
                else if (TextBoxInstansi.Text != string.Empty)
                {
                    addUniversity();
                    InputData();
                    InputDataBlockchain();
                    AfterAddUniversitas();
                    LoadMyDropDownUniv();
                }
                else
                {
                    InputData();
                    InputDataBlockchain();
                }
            }
        }

        protected void DropDownListUniversitas_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownListUniversitas.SelectedItem.Text == "OTHER")
            {
                TextBoxInstansi.Visible = true;
                TextBoxInstansi.Text = string.Empty;
            }
            else if (DropDownListUniversitas.SelectedItem.Text == "--PILIH INSTANSI--")
            {
                TextBoxInstansi.Visible = false;
                TextBoxInstansi.Text = string.Empty;
            }
            else
            {
                TextBoxInstansi.Visible = false;
                TextBoxInstansi.Text = string.Empty;
            }
        }

        public void addUniversity()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Instansi university = new Instansi
                    {
                        instansiName = TextBoxInstansi.Text.ToUpper()
                    };

                    client.BaseAddress = new Uri("http://localhost:9999/instansi/");
                    var response = client.PostAsJsonAsync("tambahInstansi", university).Result;
                    var content = response.Content.ReadAsStringAsync().Result;
                }

            }
            catch (Exception ex)
            {

            }
        }

        public void AfterAddUniversitas()
        {
            DropDownListUniversitas.Items.Clear();
            DropDownListUniversitas.Items.Add("--Pilih Instansi--");
            DropDownListUniversitas.SelectedIndex = 0;
        }
    }
}