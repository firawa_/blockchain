﻿using System;
using System.Net;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using Blockchain_New.Models;
using System.Net.Http.Formatting;
using System.Collections.Generic;
using System.Net.Mail;
using System.Security.Cryptography;

namespace Blockchain_New
{
    public partial class Aktivasi : System.Web.UI.Page
    {
        public string token { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            token = Request.QueryString["token"];
            string validatetoken = CheckToken();
            if (validatetoken == "Token Invalid")
            {
                Response.Redirect("Expired.aspx?status=1");
            }
        }

        public void ActivatedUsers()
        {
            try
            {
                string uri = "http://localhost:9999/users/activateUser/";
                var parameter = new Dictionary<string, string> { { "token", token } };
                var encodedContent = new FormUrlEncodedContent(parameter);
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(uri);
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.PostAsync(uri, encodedContent).Result;

                if (response.IsSuccessStatusCode)
                {
                    Response.Redirect("Login.aspx?status=3");
                }
            }
            catch (Exception ex)
            {
                
            }
        }

        public string CheckToken()
        {
            try
            {
                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri("http://localhost:9999/token/");
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = clients.GetAsync("validate?token=" + token).Result;

                if (response.IsSuccessStatusCode)
                {
                    return "Token Valid";
                }
                else
                {
                    return "Token Invalid";
                }

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        protected void ButtonSearch_Click(object sender, EventArgs e)
        {
            ActivatedUsers();
        }
    }
}