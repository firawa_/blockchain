﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;
using Blockchain_New.Models;
using System.Net.Http.Formatting;
using System.Collections.Generic;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Web.UI.WebControls;
using System.Diagnostics.Eventing.Reader;

namespace Blockchain_New
{
    public partial class ProfilInstansi : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["email"] == null && Session["role"] == null)
            {
                logout();
            }
            else if (Session["role"].ToString() == "ADMIN")
            {
                Response.Redirect("Home2.aspx");
            }
            else if (Session["role"].ToString() == "SUPERADMIN")
            {
                Response.Redirect("Home3.aspx");
            }
            else if (Session["role"].ToString() == "USER")
            {
                Response.Redirect("Home.aspx");
            }
            else if (Session["role"].ToString() == "PERUSAHAAN")
            {
                Response.Redirect("Homeperusahaaan.aspx");
            }
            else
            {
                GetData();
            }
        }

        public void logout()
        {
            string email = GlobalVariable.email;
            try
            {
                using (var client = new HttpClient())
                {
                    Users logout = new Users
                    {
                        email = email
                    };

                    client.BaseAddress = new Uri("http://localhost:9999/users/");
                    var response = client.PostAsJsonAsync("logout", logout).Result;
                    var content = response.Content.ReadAsStringAsync().Result;

                    if (response.IsSuccessStatusCode)
                    {
                        Session.Abandon();
                        Response.Redirect("Login.aspx");
                    }

                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Accountsetting2.aspx");
        }

        public void GetData()
        {
            try
            {
                string email = GlobalVariable.email;
                string uri = "http://localhost:9999/";
                string parameter = "users/cariUsers?email=" + email + "";
                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri(uri);
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = clients.GetAsync(parameter).Result;

                if (response.IsSuccessStatusCode)
                {
                    var content = response.Content.ReadAsStringAsync().Result;
                    var userdata = JsonConvert.DeserializeObject<Users>(content);

                    DateTime date = userdata.registeredDate;
                    DateTime time = userdata.registeredTime;

                    TextBoxName.Text = userdata.name.ToString();
                    TextBoxEmail.Text = userdata.email.ToString();
                    TextBoxPassword.Text = "**********";
                    TextBoxDate.Text = string.Format("{0:dd-MM-yyyy}", date);
                    TextBoxTime.Text = string.Format("{0:HH:mm:ss tt}", time);
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}