﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContactUs.aspx.cs" Inherits="Blockchain_New.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Contact Us</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="ContactUsstyle.css" rel="stylesheet" />
    <link rel="Icon" href="Aset/favicon.png" type="image/png" /> 
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"/>
    <script type="text/javascript">
        function HideLabel()
        {
            var seconds = 5;
            setTimeout
                (function () {
                    document.getElementById("<%=LabelBerhasil.ClientID %>").style.display = "none";
                }, seconds * 1000);
        };
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <nav class="navbar navbar-default">
      <div class="container" style="width: 98%!important">
        <div class="navbar-header"> 
<div style="align: justify; color:white; width:300px;">
<img height="100" src="Aset/Logo.jpeg" style="float: left; margin-right:9px; height:60px" width="80" /><p style="padding-top:6.8%; margin-bottom:0px!important">Blockchain based <br />Certificate Management</p></div>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="FAQ.aspx">FAQ</a></li>
            <li><a href="ContactUs.aspx">Contact Us</a></li>
            <li><a href="Login.aspx">Login/Register</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

   <div class="container">
        <div class="row">
          <div class="col-md-6" style="height:0px!important;">
          </div>

          <div class="col-md-6" style="height:40px!important;">
            <div class="panel panel-default">
                <center style="font-size:17px!important">BankSertifikat.com adalah manajemen dokumen sertifikat tepercaya. <br /> Kirimkan pertanyaan, saran, dan kritik Anda melalui form berikut.</center>
            </div>
          </div>
        </div>
      </div>
      <br />



      <div class="containerr">
        <div class="row">
          <div class="col-md-8">
          </div>

          <div class="col-md-4">
            <div class="panel panel-default">
              <div class="panel-body">
                  <div class="back">
                      <div class="dalam">
            <div class="form-group">
                <label for="NamaDepan">Nama Depan<span>*</span></label>
                <asp:TextBox CssClass="form-control" ID="TextBoxNamaDepan" runat="server" required></asp:TextBox>
            </div>
            
            <div class="form-group">
                <label for="NamaBelakang">Nama Belakang</label>
                <asp:TextBox CssClass="form-control" ID="TextBoxNamaBelakang" runat="server"></asp:TextBox>
            </div>

            <div class="form-group">
                <label for="Email">E-mail<span>*</span></label>
                <asp:TextBox CssClass="form-control" ID="TextBoxEmail" runat="server" required></asp:TextBox>
            </div>

            <div class="form-group">
                <label for="Subject">Subject<span>*</span></label>
                <asp:TextBox CssClass="form-control" ID="TextBoxSubject" runat="server" required></asp:TextBox>
            </div>
                       
            <div class="form-group">
                <label for="IsiLaporan">Isi Saran/Keluhan/Pertanyaan<span>*</span></label>
                <textarea runat="server" class="form-control" style="height:50px !important;" id="TextareaIsi" required></textarea>
            </div>

            <div class="form-group">
                     <label for="UploadLampiran">Upload Lampiran</label>
                <asp:FileUpload CssClass="form-control" style="height:40px !important;" ID="FileUploadLampiran" runat="server" />
            </div>

            <br />
            <div class="wajib"><span>*</span>Wajib Diisi</div>

            <asp:Button CssClass="btn btn-primary" ID="ButtonSubmit" runat="server" Text="Submit" OnClick="ButtonSubmit_OnClick" />

            <asp:Label ID="LabelBerhasil" runat="server"></asp:Label>
                        <br />
                      </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    </form>
</body>
</html>
