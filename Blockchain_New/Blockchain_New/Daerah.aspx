﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterpageCoba.Master" AutoEventWireup="true" CodeBehind="Daerah.aspx.cs" Inherits="Blockchain_New.Daerah" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Add Daerah</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <link href="Daerahstyle.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link rel="Icon" href="Aset/favicon.png" type="image/png" /> 
    <script src="https://kit.fontawesome.com/16b5cf1a92.js" crossorigin:"anonymous"></script>
    <script type="text/javascript">
        function HideLabel2()
        {
            var seconds = 5;
            setTimeout
                (function () {
                    document.getElementById("<%=Labelberhasil2.ClientID %>").style.display = "none";
                }, seconds * 1000);
        };
    </script>

    <script type="text/javascript">
        function HideLabel3() {
            var seconds = 5;
            setTimeout
                (function () {
                    document.getElementById("<%=Labelberhasil3.ClientID %>").style.display = "none";
                }, seconds * 1000);
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="col" style="background-color:#2d3bac; font-size:17px;">
                <div class="helpdesk" style="padding-left:30px; color:white;">
                    <br />
                    Helpdesk
                    <br />
                    <br />
                    00-xxxx-11
                    <p>
                        mail@mail.com
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />

                    </p>
                </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="add" style=" padding-left:30px; color:black; border:4px solid #2d3bac">
                <h4><strong>Add Provinsi</strong></h4>
                <div class="form-group">
                    Nama Provinsi
                        <asp:TextBox Style="text-transform:uppercase" runat="server" CssClass="box" ID="TextBoxNamaProvinsi"></asp:TextBox>
                        <asp:Button ID="ButtonAdd2" CssClass="myadd" runat="server" Text="Add" OnClick="ButtonAdd2_Click"/>
                    <p>
                        <asp:Label ID="Labelberhasil2" runat="server" CssClass="label" style="color:red; font-size:12px"></asp:Label>
                    </p>
                </div>
                <br />
                <h4><strong>Add Kabupaten/Kota</strong></h4>
                <div class="form-group">
                    Provinsi
                        <asp:DropDownList ID="DropDownListProv" runat="server" AutoPostBack ="true" AppendDataBoundItems="true" style="width:200px!important">
                            <asp:ListItem Value="">--Pilih Provinsi--</asp:ListItem>
                        </asp:DropDownList>
                </div>
                <div class="form-group">
                    Nama Kabupaten/Kota
                        <asp:TextBox Style="text-transform:uppercase" runat="server" CssClass="box" ID="TextBoxKabkota"></asp:TextBox>
                        <asp:Button ID="ButtonAdd3" CssClass="myadd" runat="server" Text="Add" OnClick="ButtonAdd3_Click"/>
                    <p style="padding-bottom:3px !important">
                        <asp:Label ID="Labelberhasil3" runat="server" CssClass="label" style="color:red; font-size:12px"></asp:Label>
                    </p>
                </div>
            </div>
          </div>
        </div>
      </div>
    <br />
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="col">
                <asp:GridView ID="ListProvinsi" CssClass="datauniversitas" runat="server" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:BoundField DataField="provinceId" HeaderText="Kode Provinsi" />
                                    <asp:BoundField DataField="provinceName" HeaderText="Nama Provinsi" />
						            <asp:TemplateField>
							            <ItemTemplate>
							               <asp:LinkButton runat="server" CssClass="mydetail2" ID="Show" Text="Show" OnClick = "Show_City_Click"></asp:LinkButton>
							            </ItemTemplate>
						            </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" CssClass="mydetail" ID="Deleteprov" Text="Delete" OnClick = "Delete_Provinsi_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                </asp:GridView>
            </div>
          </div>

          <div class="col-md-6">
            <div class="col">
                <asp:GridView ID="ListDaerah" CssClass="datauniversitas" runat="server" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:BoundField DataField="cityName" HeaderText="Nama Kabupaten/Kota" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" CssClass="mydetail" ID="Deletekabkota" Text="Delete" OnClick = "Delete_Kota_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                </asp:GridView>
            </div>
          </div>
        </div>
      </div>
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
</asp:Content>
