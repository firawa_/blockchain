﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FAQ.aspx.cs" Inherits="Blockchain_New.FAQ" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>FAQ</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="FAQstyle.css" rel="stylesheet" />
    <link rel="Icon" href="Aset/favicon.png" type="image/png" /> 
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"/>
    <script src="https://kit.fontawesome.com/16b5cf1a92.js" crossorigin:"anonymous"></script>
</head>
<body>
    <form id="form1" runat="server">
    <nav class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
<div style="align: justify; color:white; width:300px; margin-left:0%">
<img height="100" src="Aset/Logo.jpeg" style="float: left; margin-right:9px; height:60px" width="80" /><p style="padding-top:6.8%; margin-bottom:0px!important; font-size:16px !important">Blockchain based <br />Certificate Management</p></div>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="FAQ.aspx">FAQ</a></li>
            <li><a href="ContactUs.aspx">Contact Us</a></li>
            <li><a href="Login.aspx">Login/Register</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
<br />
<br />

<div class="container">
   <div class="row">
       <div class="col-md-7">
       </div>

       <div class="col-md-5">
            <div class="FAQ" style="background-color:black">
            <section class="accordion-section clearfix mt-3" aria-label="Question Accordions">
              <div class="container">
  
	              <h2>Pertanyaan yang Sering Diajukan</h2>
                  <br />
	              <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
		            <div class="panel panel-default">
		              <div class="panel-heading p-3 mb-3" role="tab" id="heading0">
			            <h3 class="panel-title">
			              <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse0" aria-expanded="true" aria-controls="collapse0">
				            Untuk apakah website ini dibuat?
			              </a>
			            </h3>
		              </div>
		              <div id="collapse0" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading0">
			            <div class="panel-body px-3 mb-4">
			              <p>Website ini dibuat untuk penyimpanan dokumen dengan teknologi blockchain.</p>
			            </div>
		              </div>
		            </div>
		
                    <br />
		            <div class="panel panel-default">
		              <div class="panel-heading p-3 mb-3" role="tab" id="heading1">
			            <h3 class="panel-title">
			              <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
				            Siapakah pengelola website ini?
			              </a>
			            </h3>
		              </div>
		              <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
			            <div class="panel-body px-3 mb-4">
			              <p>Perusahaan bernama Bisnis Tekno Utama.</p>
			            </div>
		              </div>
		            </div>
		
                    <br />
		            <div class="panel panel-default">
		              <div class="panel-heading p-3 mb-3" role="tab" id="heading2">
			            <h3 class="panel-title">
			              <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
				            Sudah banyak protokol pengaman jaringan. Apa yang baru dari <br /> blockchain yang absen di protokol-protokol lain untuk pengaman jaringan?
			              </a>
			            </h3>
		              </div>
		              <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
			            <div class="panel-body px-3 mb-4">
			              <p>Silahkan lihat <a style="color:white!important" href="https://youtu.be/KBSGLJ1grSs">di sini.</a></p>
			            </div>
		              </div>
		            </div>
		
                    <br />
		            <div class="panel panel-default">
		              <div class="panel-heading p-3 mb-3" role="tab" id="heading3">
			            <h3 class="panel-title">
			              <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3">
				            Apakah BTU memakai public miner atau private miner? 
			              </a>
			            </h3>
		              </div>
		              <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
			            <div class="panel-body px-3 mb-4">
			              <p>BTU memakai public miner.</p>
			            </div>
		              </div>
		            </div>
	              </div>

                    <br />
		            <div class="panel panel-default">
		              <div class="panel-heading p-3 mb-3" role="tab" id="heading4">
			            <h3 class="panel-title">
			              <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3">
				            Apa perbedaan Instansi dengan Perusahaan pada form pendaftaran?
			              </a>
			            </h3>
		              </div>
		              <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
			            <div class="panel-body px-3 mb-4">
			              <p>Instansi adalah untuk organisasi yang keluarkan sertifikat. <br /> Contoh: Sekolah, Lembaga Sertifikasi Profesi. Perusahaan adalah organisasi-organisasi di luar sekolah dan lembaga sertifikasi profesi.</p>
			            </div>
		              </div>
		            </div>
	              </div>
  
              </div>
            </section>
            </div>
        </div>
    </div>
 </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    </form>
</body>
</html>
