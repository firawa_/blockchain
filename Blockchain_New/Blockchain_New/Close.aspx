﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterpageCoba.Master" AutoEventWireup="true" CodeBehind="Close.aspx.cs" Inherits="Blockchain_New.Close" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Report</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <link href="Closestyle.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link rel="Icon" href="Aset/favicon.png" type="image/png" /> 
    <script src="https://kit.fontawesome.com/16b5cf1a92.js" crossorigin:"anonymous"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<br />
    <asp:GridView ID="ReportData" CssClass="mydata" runat="server" AutoGenerateColumns="False">
					<Columns>
						<asp:BoundField DataField="idLaporan" HeaderText="Id Laporan" />
						<asp:BoundField DataField="email" HeaderText="E-mail Pengirim" />
						<asp:BoundField DataField="tanggalLaporan" HeaderText="Tanggal Laporan" HtmlEncode="false" DataFormatString="{0:MM/dd/yyyy}" />	
                        <asp:BoundField DataField="judulLaporan" HeaderText="Subjek" />
                        <asp:TemplateField>
							<ItemTemplate>
							   <asp:LinkButton runat="server" CssClass="mydetail" ID="DetailLaporan" Text="Detail" OnClick = "Detail_Click"></asp:LinkButton>
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
    </asp:GridView>
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
</asp:Content>
