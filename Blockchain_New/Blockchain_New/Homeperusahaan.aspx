﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterpagePerusahaan.Master" AutoEventWireup="true" CodeBehind="Homeperusahaan.aspx.cs" Inherits="Blockchain_New.Homeperusahaan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Home Perusahaan</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <link href="Homestyle.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link rel="Icon" href="Aset/favicon.png" type="image/png" /> 
    <script src="https://kit.fontawesome.com/16b5cf1a92.js" crossorigin:"anonymous"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<br />
    <asp:GridView ID="Data" CssClass="mydata" runat="server" AutoGenerateColumns="False">
					<Columns>
						<asp:BoundField DataField="idData" HeaderText="Id" />
						<asp:BoundField DataField="indexBlock" HeaderText="Index" />
						<asp:BoundField DataField="nonce" HeaderText="Nonce" />
						<asp:BoundField DataField="timestamp" HeaderText="Time" />
						<asp:BoundField DataField="previousHash" HeaderText="Previous Hash" />
						<asp:TemplateField>
							<ItemTemplate>
							   <asp:LinkButton runat="server" CssClass="mydetail" ID="Detail" Text="Detail" OnClick ="Detail_Click"></asp:LinkButton>
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
    </asp:GridView>
</asp:Content>
