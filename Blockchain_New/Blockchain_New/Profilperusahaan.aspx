﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterpagePerusahaan.Master" AutoEventWireup="true" CodeBehind="Profilperusahaan.aspx.cs" Inherits="Blockchain_New.Profil" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Profil</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <link href="profilstyle.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link rel="Icon" href="Aset/favicon.png" type="image/png" /> 
    <script src="https://kit.fontawesome.com/16b5cf1a92.js" crossorigin:"anonymous"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<br />
      <div class="container">
        <div class="row">
          <div class="col-md-2">
          </div>

          <div class="col-md-8">
            <div class="form-group row">
              <label class="col-2 col-form-label">Name</label>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <asp:TextBox runat="server" value="" ID="TextBoxName" ReadOnly="true" style="width:60% !important;" Enabled="false"></asp:TextBox>
            </div>
            <div class="form-group row">
              <label class="col-2 col-form-label">E-mail</label>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <asp:TextBox runat="server" value="" ID="TextBoxEmail" ReadOnly="true" style="width:60% !important;" Enabled="false"></asp:TextBox>
            </div>
            <div class="form-group row">
              <label class="col-2 col-form-label">Password</label>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <asp:TextBox runat="server" value="" ID="TextBoxPassword" ReadOnly="true" style="width:60% !important;" Enabled="false"></asp:TextBox>
              <asp:Button ID="Button1" runat="server" Text="Change Password" OnClick="Button1_Click"/>
            </div> 
            <div class="form-group row">
              <label class="col-2 col-form-label">Registered Date</label>
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <asp:TextBox runat="server" value="" ID="TextBoxDate" ReadOnly="true" style="width:60% !important;" Enabled="false"></asp:TextBox>
            </div>
            <div class="form-group row">
              <label class="col-2 col-form-label">Registered Time</label>
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <asp:TextBox runat="server" value="" ID="TextBoxTime" ReadOnly="true" style="width:60% !important;" Enabled="false"></asp:TextBox>
            </div>  
          <br />
          <br />
          <br />
          <br />           
          </div>


          <div class="col-md-2">
          </div>
        </div>
      </div>
</asp:Content>

