﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterpageSuperadmin.Master" AutoEventWireup="true" CodeBehind="Create.aspx.cs" Inherits="Blockchain_New.Create" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Create New Admin</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <link href="Accountsettingstyle.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"/>
    <script src="https://kit.fontawesome.com/16b5cf1a92.js" crossorigin:"anonymous"></script>
    <script type="text/javascript">
        function HideLabel()
        {
            var seconds = 5;
            setTimeout
                (function () {
                    document.getElementById("<%=LabelBerhasil.ClientID %>").style.display = "none";
                }, seconds * 1000);
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<br />
    <center style="font-size:25px;"><img src="Aset/images.png" />&nbsp;Create New Admin</center>
      <div class="container">
        <div class="row">
          <div class="col-md-2">

          </div>

          <div class="col-md-8">
          <div class="colmd8">
            <div class="row">
            <div class="col-25">
                <label for="Namaadmin">Nama&nbsp;Admin</label>
            </div>
            <div class="col-75">
                <asp:TextBox Style="text-transform:uppercase"  Class="form-control" ID="TextBoxNamaadmin" runat="server" required MaxLength="40"></asp:TextBox>
            </div>
            </div>
            <div class="row">
            <div class="col-25">
                <label for="user">E-mail</label>
            </div>
            <div class="col-75">
                <asp:TextBox Class="form-control" ID="TextBoxUsername" runat="server" required MaxLength="40"></asp:TextBox>
            </div>
            </div>
            <div class="row">
            <div class="col-25">
                <label for="pass">Password</label>
            </div>
            <div class="col-75">
                <asp:TextBox Class="form-control" ID="TextBoxPassword" runat="server" TextMode="Password" required></asp:TextBox>
            </div>
            </div>
            <br />
            <div class="row">
                <asp:Button class="ubah" ID="ButtonSubmit" runat="server" Text="Add" OnClick="ButtonRegister_Click"/>
                <asp:Label ID="LabelBerhasil" runat="server" style="margin-left:10px; color:red; font-size:13px;"></asp:Label>
            </div>
            <br />
          </div>
            <br />
            <br />
            <br />
            <br />
          </div>

          <div class="col-md-2">

          </div>
        </div>
      </div>
</asp:Content>
