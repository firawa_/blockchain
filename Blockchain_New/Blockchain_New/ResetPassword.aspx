﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResetPassword.aspx.cs" Inherits="Blockchain_New.ResetPassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Reset Password</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <link href="Accountsettingstyle.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link rel="Icon" href="Aset/favicon.png" type="image/png" /> 
    <script src="https://kit.fontawesome.com/16b5cf1a92.js" crossorigin:"anonymous"></script> 
    <script type="text/javascript">
        function HideLabel()
        {
            var seconds = 5;
            setTimeout
                (function () {
                    document.getElementById("<%=LabelBerhasil.ClientID %>").style.display = "none";
                }, seconds * 1000);
        };
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <header id="header" style="background-color: #0d1463; color:#ffffff; padding-bottom: 10px; padding-left:5px;">
            <div class="row">
              <div class="col-md-10" style="font-size:40px !important; padding-top:18px; padding-bottom:10px;">
<div style="align: justify; color:white; width:100%; font-size:23px">
<img height="100" src="Aset/Logo.jpeg" style="float: left; margin-right:9px; height:60px" width="80" />Blockchain based <br />Certificate Management</div>
              </div>
            </div>
        </header>
      <div class="container">
        <div class="row">
          <div class="col-md-2">

          </div>

          <div class="col-md-8">
            <h3>Change Your Password</h3>
            <br />
            <div class="col-25">
                <label for="new">New Password</label>
            </div>
            <div class="col-75">
                <asp:TextBox Class="form-control" ID="TextBoxNew" runat="server" TextMode="Password" required></asp:TextBox>
            </div>
              <br />
            <div class="col-25">
                <label for="confirm">Confirm new Password</label>
            </div>
            <div class="col-75">
                <asp:TextBox Class="form-control" ID="TextBoxConfirm" runat="server" TextMode="Password" required></asp:TextBox>
            </div>
            <br />
            <div class="row">
                <asp:Button class="ubah" ID="ButtonSearch" runat="server" Text="Change Your Password" style="width:200px!important" OnClick="ButtonSearch_Click"/>
                <asp:Label ID="LabelBerhasil" runat="server" style="margin-left:10px; color:red; font-size:13px;"></asp:Label>
                <a style="float: right; padding-right:15%" href="Login.aspx">Login</a>
            </div>
            <br />
          </div>
          </div>

          <div class="col-md-2">

          </div>
        </div>
      </div>

    </form>
</body>
</html>
