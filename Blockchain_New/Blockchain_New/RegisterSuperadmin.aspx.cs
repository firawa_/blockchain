﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;
using Blockchain_New.Models;
using System.Net.Http.Formatting;
using System.Collections.Generic;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

namespace Blockchain_New
{
    public partial class RegisterSuperadmin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonRegister_Click(object sender, EventArgs e)
        {
            DoRegister();
        }

        public void DoRegister()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Users user = new Users
                    {
                        email = TextBoxUsername.Text.Trim(),
                        password = TextBoxPassword.Text,
                        channel = 999999,
                        name = TextBoxNamasuperadmin.Text.ToUpper().Trim(),
                        activated = true
                    };

                    var hasNumber = new Regex(@"[0-9]+");
                    var hasUpperChar = new Regex(@"[A-Z]+");
                    var hasMiniMaxChars = new Regex(@".{8,32}");
                    var hasLowerChar = new Regex(@"[a-z]+");
                    var hasSymbols = new Regex(@"[!@#$%^&*()_+=\[{\]};:<>|./?,-]");
                    var email = new Regex(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");

                    var pass = TextBoxPassword.Text;
                    var uname = TextBoxUsername.Text;

                    if (!hasLowerChar.IsMatch(pass))
                    {
                        Label1RegisterSuperadmin.Visible = true;
                        Label1RegisterSuperadmin.ForeColor = System.Drawing.Color.Red;
                        Label1RegisterSuperadmin.Text = "Your password should contain at least one lower case letter.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else if (!hasUpperChar.IsMatch(pass))
                    {
                        Label1RegisterSuperadmin.Visible = true;
                        Label1RegisterSuperadmin.ForeColor = System.Drawing.Color.Red;
                        Label1RegisterSuperadmin.Text = "Your password should contain at least one upper case letter.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else if (!hasMiniMaxChars.IsMatch(pass))
                    {
                        Label1RegisterSuperadmin.Visible = true;
                        Label1RegisterSuperadmin.ForeColor = System.Drawing.Color.Red;
                        Label1RegisterSuperadmin.Text = "Your password should not be less than 8 characters.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else if (!hasNumber.IsMatch(pass))
                    {
                        Label1RegisterSuperadmin.Visible = true;
                        Label1RegisterSuperadmin.ForeColor = System.Drawing.Color.Red;
                        Label1RegisterSuperadmin.Text = "Your password should contain at least one numeric value.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else if (!hasSymbols.IsMatch(pass))
                    {
                        Label1RegisterSuperadmin.Visible = true;
                        Label1RegisterSuperadmin.ForeColor = System.Drawing.Color.Red;
                        Label1RegisterSuperadmin.Text = "Your password should contain at least one special case characters.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else if (!email.IsMatch(uname))
                    {
                        Label1RegisterSuperadmin.Visible = true;
                        Label1RegisterSuperadmin.ForeColor = System.Drawing.Color.Red;
                        Label1RegisterSuperadmin.Text = "Your email is not valid.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else if (TextBoxUsername.Text.Trim() == TextBoxPassword.Text.Trim())
                    {
                        Label1RegisterSuperadmin.Visible = true;
                        Label1RegisterSuperadmin.ForeColor = System.Drawing.Color.Red;
                        Label1RegisterSuperadmin.Text = "Your password cannot be the same as your username.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else
                    {
                        client.BaseAddress = new Uri("http://localhost:9999/superadmin/");
                        var response = client.PostAsJsonAsync("register", user).Result;
                        var s = response.Content.ReadAsStringAsync().Result;

                        if (response.IsSuccessStatusCode)
                        {
                            TextBoxUsername.Text = string.Empty;
                            TextBoxPassword.Text = string.Empty;
                            TextBoxNamasuperadmin.Text = string.Empty;

                            Label1RegisterSuperadmin.Visible = true;
                            Label1RegisterSuperadmin.ForeColor = System.Drawing.Color.Green;
                            Label1RegisterSuperadmin.Text = "Superadmin is successful to register.";
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                        }
                        else
                        {
                            Label1RegisterSuperadmin.Visible = true;
                            Label1RegisterSuperadmin.ForeColor = System.Drawing.Color.Red;
                            Label1RegisterSuperadmin.Text = "Email address has been used.";
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Label1RegisterSuperadmin.Visible = true;
                Label1RegisterSuperadmin.ForeColor = System.Drawing.Color.Red;
                Label1RegisterSuperadmin.Text = ex.Message;
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
            }
        }
    }
}