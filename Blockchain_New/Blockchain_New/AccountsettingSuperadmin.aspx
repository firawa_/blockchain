﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterpageSuperadmin.Master" AutoEventWireup="true" CodeBehind="AccountsettingSuperadmin.aspx.cs" Inherits="Blockchain_New.AccountsettingSuperadmin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Setting</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <link href="Accountsettingstyle.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link rel="Icon" href="Aset/favicon.png" type="image/png" /> 
    <script src="https://kit.fontawesome.com/16b5cf1a92.js" crossorigin:"anonymous"></script>
    <script type="text/javascript">
        function HideLabel()
        {
            var seconds = 5;
            setTimeout
                (function () {
                    document.getElementById("<%=LabelBerhasil.ClientID %>").style.display = "none";
                }, seconds * 1000);
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <div class="container">
        <div class="row">
          <div class="col-md-2">

          </div>

          <div class="col-md-8">
          <div class="colmd8">
          <h3>Change Password</h3>

            <div class="col-25">
                <label for="fname">Old Password</label>
            </div>
            <div class="col-75">
                <asp:TextBox Class="form-control" ID="TextBoxPasswordlama" runat="server" TextMode="Password" required></asp:TextBox>
            </div>
            <div class="row">
                <div class="col-25">
                    <label for="lname">New Password</label>
                </div>
                <div class="col-75">
                    <asp:TextBox Class="form-control" ID="TextBoxPasswordbaru" runat="server" TextMode="Password" required></asp:TextBox>
                </div>
            </div>
            <div class="row">
                <div class="col-25">
                    <label for="lname">Confirm New Password</label>
                </div>
                <div class="col-75">
                    <asp:TextBox Class="form-control" ID="TextBoxPasswordulangi" runat="server" TextMode="Password" required></asp:TextBox>
                </div>
            </div>
            <br />
            <div class="row">
                <asp:Button class="ubah" ID="ButtonUbah" runat="server" Text="Change" OnClick="ButtonUbah_Click"/>
                <asp:Label ID="LabelBerhasil" runat="server" style="margin-left:10px; color:red; font-size:13px;"></asp:Label>
            </div>
            <br />
          </div>
            <br />
            <br />
            <br />
            <br />
          </div>

          <div class="col-md-2">

          </div>
        </div>
      </div>
</asp:Content>
