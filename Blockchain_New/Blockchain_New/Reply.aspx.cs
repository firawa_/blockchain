﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;
using Blockchain_New.Models;
using System.Net.Http.Formatting;
using System.Collections.Generic;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Web.UI.WebControls;
using System.IO;

namespace Blockchain_New
{
    public partial class Reply : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["email"] == null && Session["role"] == null)
            {
                logout();
            }
            else if (Session["role"].ToString() == "USER")
            {
                Response.Redirect("Home.aspx");
            }
            else if (Session["role"].ToString() == "SUPERADMIN")
            {
                Response.Redirect("Home3.aspx");
            }
            else if (Session["role"].ToString() == "INSTANSI")
            {
                Response.Redirect("Home4.aspx");
            }
            else if (Session["role"].ToString() == "PERUSAHAAN")
            {
                Response.Redirect("Homeperusahaaan.aspx");
            }
            else
            {
                GetData();
            }
        }

        public void logout()
        {
            string email = GlobalVariable.email;
            try
            {
                using (var client = new HttpClient())
                {
                    Users logout = new Users
                    {
                        email = email
                    };

                    client.BaseAddress = new Uri("http://localhost:9999/users/");
                    var response = client.PostAsJsonAsync("logout", logout).Result;
                    var content = response.Content.ReadAsStringAsync().Result;

                    if (response.IsSuccessStatusCode)
                    {
                        Session.Abandon();
                        Response.Redirect("Login.aspx");
                    }

                }
            }
            catch (Exception ex)
            {

            }
        }

        public void GetData()
        {
            TextBoxEmail.Text = GlobalVariable.purposeEmail;
            TextBoxSubject.Text = GlobalVariable.subjectEmail;
            TextareaComplain.Value = GlobalVariable.contentEmail;
            TextBoxEmail.Enabled = false;
            TextBoxSubject.Enabled = false;
            TextareaComplain.Disabled = true;
        }

        public string CheckFileUpload()
        {
            if (FileUploadLampiran.HasFile)
            {
                string File = Server.MapPath(FileUploadLampiran.PostedFile.FileName);
                return File;
            }
            else
            {
                string File = "";
                return File;
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            string File = CheckFileUpload();

            try
            {
                using (var client = new HttpClient())
                {
                    Email mailaddres = new Email
                    {
                        mailFrom = Session["email"].ToString(),
                        mailTo = TextBoxEmail.Text,
                        mailSubject = TextBoxSubject.Text,
                        mailContent = Textarea.Value,
                        attachment = File
                    };

                    client.BaseAddress = new Uri("http://localhost:9999/mail/");
                    var response = client.PostAsJsonAsync("sendMail", mailaddres).Result;
                    var content = response.Content.ReadAsStringAsync().Result;

                    if (response.IsSuccessStatusCode)
                    {
                        CloseReport();
                        if (Label1.Text == "Success")
                        {
                            Label1.Visible = true;
                            Label1.ForeColor = System.Drawing.Color.Green;
                            Label1.Text = "Email has been succesfully to send.";
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                        }

                        Textarea.Value = string.Empty;
                    }
                    else
                    {
                        Label1.Visible = true;
                        Label1.ForeColor = System.Drawing.Color.Red;
                        Label1.Text = "Failed to send Email.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                Label1.Visible = true;
                Label1.Text = ex.Message;
            }
        }

        public void CloseReport()
        {
            string idLaporanClose = Session["idLaporan"].ToString();
            try
            {
                using (var client = new HttpClient())
                {
                    Laporan closeReport = new Laporan()
                    {
                        idLaporan = Convert.ToInt32(idLaporanClose)
                    };

                    client.BaseAddress = new Uri("http://localhost:9999/laporan/");
                    var response = client.PostAsJsonAsync("closeLaporan", closeReport).Result;
                    var content = response.Content.ReadAsStringAsync().Result;

                    if (response.IsSuccessStatusCode)
                    {
                        Label1.Visible = true;
                        Label1.Text = "Success";
                    }
                    else
                    {
                        Label1.Visible = true;
                        Label1.Text = "Failed";
                    }
                }
            }
            catch (Exception ex)
            {
                Label1.Visible = true;
                Label1.Text = ex.Message;
            }
        }
    }
}