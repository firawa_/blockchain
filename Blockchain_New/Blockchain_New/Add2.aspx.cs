﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;
using Blockchain_New.Models;
using System.Net.Http.Formatting;
using System.Collections.Generic;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Web.UI.WebControls;
using System.Runtime.CompilerServices;
using System.Linq;
using System.Text.RegularExpressions;
using System.IO;

namespace Blockchain_New
{
    public partial class Add2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            TextBoxInstansi.Text = GlobalVariable.name;

            if (Session["email"] == null && Session["role"] == null)
            {
                logout();
            }
            else if (Session["role"].ToString() == "ADMIN")
            {
                Response.Redirect("Home2.aspx");
            }
            else if (Session["role"].ToString() == "SUPERADMIN")
            {
                Response.Redirect("Home3.aspx");
            }
            else if (Session["role"].ToString() == "USER")
            {
                Response.Redirect("Home.aspx");
            }
            else if (Session["role"].ToString() == "PERUSAHAAN")
            {
                Response.Redirect("Homeperusahaaan.aspx");
            }
            else
            {
                if (!Page.IsPostBack)
                {
                    LoadMyDropDownProv();
                }
            }
        }

        public void logout()
        {
            string email = GlobalVariable.email;
            try
            {
                using (var client = new HttpClient())
                {
                    Users logout = new Users
                    {
                        email = email
                    };

                    client.BaseAddress = new Uri("http://localhost:9999/users/");
                    var response = client.PostAsJsonAsync("logout", logout).Result;
                    var content = response.Content.ReadAsStringAsync().Result;

                    if (response.IsSuccessStatusCode)
                    {
                        Session.Abandon();
                        Response.Redirect("Login.aspx");
                    }

                }
            }
            catch (Exception ex)
            {

            }
        }

        public void LoadMyDropDownProv()
        {
            string email = Session["email"].ToString();

            if (email != null)
            {
                try
                {
                    HttpClient clients = new HttpClient();
                    clients.BaseAddress = new Uri("http://localhost:9999/provinsi/");
                    clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage response = clients.GetAsync("daftarProvinsi").Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        List<Provinsi> provinsiList = (new JavaScriptSerializer()).Deserialize<List<Provinsi>>(content);
                        DropDownListProvinsi.DataSource = provinsiList;
                        DropDownListProvinsi.DataTextField = "provinceName";
                        DropDownListProvinsi.DataValueField = "provinceId";
                        DropDownListProvinsi.DataBind();
                        DropDownListProvinsi.Items.Add("OTHER");
                    }

                }
                catch (Exception ex)
                {

                }
            }
        }

        public void LoadMyDropDownKota()
        {
            string email = Session["email"].ToString();

            if (email != null)
            {
                try
                {
                    string id = DropDownListProvinsi.SelectedItem.Value.ToString();
                    string uri = "http://localhost:9999/";
                    string parameter = "kota/daftarKotaByProvince?provinceId=" + id + "";
                    HttpClient clients = new HttpClient();
                    clients.BaseAddress = new Uri(uri);
                    clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage response = clients.GetAsync(parameter).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        List<Kota> kotaList = (new JavaScriptSerializer()).Deserialize<List<Kota>>(content);
                        DropDownListKabkota.DataSource = kotaList;
                        DropDownListKabkota.DataTextField = "cityName";
                        DropDownListKabkota.DataValueField = "cityId";
                        DropDownListKabkota.DataBind();
                        DropDownListKabkota.Items.Add("OTHER");
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }

        protected void DropDownListProvinsi_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownListProvinsi.SelectedItem.Text == "OTHER")
            {
                DropDownListKabkota.Items.Clear();
                DropDownListKabkota.Items.Add("OTHER");
                TextBoxKotaKab.Visible = true;
                TextBoxKotaKab.Text = string.Empty;
            }
            else if (DropDownListProvinsi.SelectedItem.Text == "--PILIH PROVINSI--")
            {
                TextBoxProvinsi.Visible = false;
                DropDownListKabkota.Items.Clear();
                DropDownListKabkota.Items.Add("--PILIH KAB/KOTA--");
                TextBoxKotaKab.Visible = false;
            }
            else
            {
                TextBoxProvinsi.Visible = false;
                TextBoxKotaKab.Visible = false;
                DropDownListKabkota.Items.Clear();
                DropDownListKabkota.Items.Add("--PILIH KAB/KOTA--");
                LoadMyDropDownKota();
            }
        }

        protected void DropDownListKabkota_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownListKabkota.SelectedItem.Text == "OTHER")
            {
                TextBoxKotaKab.Visible = true;
                TextBoxKotaKab.Text = string.Empty;
            }
            else if (DropDownListKabkota.SelectedItem.Text == "--PILIH KAB/KOTA--")
            {
                TextBoxKotaKab.Visible = false;
                TextBoxKotaKab.Text = string.Empty;
            }
            else
            {
                TextBoxKotaKab.Visible = false;
                TextBoxKotaKab.Text = string.Empty;
            }
        }


        public string CheckDataKabKota()
        {
            if (DropDownListProvinsi.SelectedItem.Text != "OTHER")
            {
                string kabkota = DropDownListKabkota.SelectedItem.Text;
                return kabkota;
            }
            else
            {
                string kabkota = TextBoxKotaKab.Text.ToUpper();
                return kabkota;
            }
        }

        public string GetRandomName()
        {
            string extention = System.IO.Path.GetExtension(FileUploadIjazah.FileName);
            if (extention == ".pdf")
            {
                string random_name = Convert.ToString(Guid.NewGuid()) + ".pdf";
                return random_name;
            }
            else
            {
                string random_name = Convert.ToString(Guid.NewGuid()) + ".jpg";
                return random_name;
            }
        }

        UserData userData = new UserData();

        public void InputData()
        {
            string email = Session["email"].ToString();
            string kabkota = CheckDataKabKota();
            string random_name = GetRandomName();
            if (email != null)
            {
                try
                {
                    string Image = FileUploadIjazah.FileName.ToString();
                    string extention = System.IO.Path.GetExtension(FileUploadIjazah.FileName);
                    if (extention == ".pdf")
                    {
                        FileUploadIjazah.PostedFile.SaveAs(Server.MapPath("~/Upload/PDF/" + random_name));
                    }
                    else
                    {
                        FileUploadIjazah.PostedFile.SaveAs(Server.MapPath("~/Upload/Image/" + random_name));
                    }
                    using (var client = new HttpClient())
                    {
                        userData.email = TextBoxEmail.Text;
                        userData.gelarBelakang = TextBoxGelarbelakang.Text;
                        userData.gelarDepan = TextBoxGelardepan.Text;
                        userData.nama = TextBoxNamalengkap.Text;
                        userData.ttl = kabkota + ", " + TextBoxTanggallahir.Text;
                        userData.noIjazah = TextBoxNomorijazah.Text.Trim();
                        userData.scanFile = random_name;
                        userData.tglIjazah = DateTime.Parse(TextBoxTanggaldikeluarkanijazah.Text).Date;
                        userData.instansi = TextBoxInstansi.Text;
                        userData.thnIjazah = int.Parse(TextBoxTahunterbitijazah.Text.Trim());
                        userData.picEmail = email;

                        client.BaseAddress = new Uri("http://localhost:9999/userdata/");
                        var response = client.PostAsJsonAsync("tambahUserData", userData).Result;
                        var content = response.Content.ReadAsStringAsync().Result;

                        if (response.IsSuccessStatusCode)
                        {
                            var users = JsonConvert.DeserializeObject<UserData>(content);

                            userData.idData = users.idData;
                        }
                        else
                        {
                            Labelsukses.Visible = true;
                            Labelsukses.Text = "Error Input Data"; 
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                        }
                    }

                }
                catch (Exception ex)
                {
                    Labelsukses.Visible = true;
                    Labelsukses.Text = ex.Message;
                }
            }
        }

        public void InputDataBlockchain()
        {
            if (Labelsukses.Text == "Error")
            {
                return;
            }

            string email = Session["email"].ToString();
            if (email != null)
            {
                try
                {
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri("http://localhost:9999/blockchain/");
                        var response = client.PostAsJsonAsync("tambahBlockchain", userData).Result;
                        var content = response.Content.ReadAsStringAsync().Result;

                        if (response.IsSuccessStatusCode)
                        {
                            TextBoxGelardepan.Text = string.Empty;
                            TextBoxNamalengkap.Text = string.Empty;
                            TextBoxGelarbelakang.Text = string.Empty;
                            TextBoxTanggallahir.Text = string.Empty;
                            TextBoxNomorijazah.Text = string.Empty;
                            TextBoxTahunterbitijazah.Text = string.Empty;
                            TextBoxTanggaldikeluarkanijazah.Text = string.Empty;
                            TextBoxKotaKab.Visible = false;
                            TextBoxKotaKab.Text = string.Empty;
                            DropDownListProvinsi.SelectedIndex = 0;
                            DropDownListKabkota.Items.Clear();
                            DropDownListKabkota.Items.Add("--PILIH KAB/KOTA--");
                            DropDownListKabkota.SelectedIndex = 0;
                            FileUploadIjazah.Attributes.Clear();
                            TextBoxEmail.Text = string.Empty;

                            Labelsukses.Visible = true;
                            Labelsukses.ForeColor = System.Drawing.Color.Green;
                            Labelsukses.Text = "Data added successfully.";
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Labelsukses.Visible = true;
                    Labelsukses.Text = ex.Message;
                }
            }
        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            int number;
            bool isNumeric = int.TryParse(TextBoxTahunterbitijazah.Text, out number);
            string extention_file = System.IO.Path.GetExtension(FileUploadIjazah.FileName);
            string[] list_extention = { ".jpg", ".jpeg", ".png", ".pdf"};

            var email = new Regex(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
            var uname = TextBoxEmail.Text;

            if (!list_extention.Contains(extention_file))
            {
                Labelsukses.Visible = true;
                Labelsukses.Text = "Your file must an image or a pdf.";
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
            }
            else if (!email.IsMatch(uname))
            {
                Labelsukses.Visible = true;
                Labelsukses.Text = "Your email is not valid.";
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
            }
            else if (isNumeric == false)
            {
                Labelsukses.Visible = true;
                Labelsukses.Text = "Year of publication must be an integer number.";
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
            }
            else
            {
                InputData();
                InputDataBlockchain();
            }
        }
    }
}