﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;
using Blockchain_New.Models;
using System.Net.Http.Formatting;
using System.Collections.Generic;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Web.UI.WebControls;
using System.IO;

namespace Blockchain_New
{
    public partial class DetailLaporan : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["email"] == null && Session["role"] == null)
            {
                logout();
            }
            else if (Session["role"].ToString() == "USER")
            {
                Response.Redirect("Home.aspx");
            }
            else if (Session["role"].ToString() == "SUPERADMIN")
            {
                Response.Redirect("Home3.aspx");
            }
            else if (Session["role"].ToString() == "INSTANSI")
            {
                Response.Redirect("Home4.aspx");
            }
            else if (Session["role"].ToString() == "PERUSAHAAN")
            {
                Response.Redirect("Homeperusahaaan.aspx");
            }
            else
            {
                string statusLaporan = Session["statusLaporan"].ToString();

                if (statusLaporan == "OPEN")
                {
                    Button3.Visible = true;
                }
                else
                {
                    Button3.Visible = false;
                }

                GetData();
            }    
        }

        public void logout()
        {
            string email = GlobalVariable.email;
            try
            {
                using (var client = new HttpClient())
                {
                    Users logout = new Users
                    {
                        email = email
                    };

                    client.BaseAddress = new Uri("http://localhost:9999/users/");
                    var response = client.PostAsJsonAsync("logout", logout).Result;
                    var content = response.Content.ReadAsStringAsync().Result;

                    if (response.IsSuccessStatusCode)
                    {
                        Session.Abandon();
                        Response.Redirect("Login.aspx");
                    }

                }
            }
            catch (Exception ex)
            {

            }
        }

        public void GetData()
        {
            string id = Session["idLaporan"].ToString();
            try
            {
                string uri = "http://localhost:9999/";
                string parameter = "laporan/cariLaporan?idLaporan=" + id + "";
                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri(uri);
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = clients.GetAsync(parameter).Result;

                if (response.IsSuccessStatusCode)
                {
                    var content = response.Content.ReadAsStringAsync().Result;
                    var report = JsonConvert.DeserializeObject<Laporan>(content);

                    TextBoxIDLaporan.Text = report.idLaporan.ToString();
                    TextBoxNamadepan.Text = report.namaDepan.ToString();
                    TextBoxNamabelakang.Text = report.namaBelakang.ToString();
                    TextBoxEmail.Text = report.email.ToString();
                    TextBoxTanggallaporan.Text = report.tanggalLaporan.ToString("dd/MM/yyy");
                    TextBoxSubjeklaporan.Text = report.judulLaporan.ToString();
                    TextareaIsi.Value = report.isiLaporan.ToString();
                    LabelStatus.Text = report.statusLaporan.ToString();

                    string extention = System.IO.Path.GetExtension(report.lampiranLaporan.ToString());

                    if (report.lampiranLaporan.ToString() == "")
                    {
                        Lampiran.ImageUrl = "~/Lampiran/NoAvailable/No_Image_Available.png";
                        Lampiran.Enabled = false;

                        FileStream file = new FileStream(MapPath("~/Lampiran/NoAvailable/No_Image_Available.png"), FileMode.Open, FileAccess.Read, FileShare.Read);
                        System.Drawing.Image img = System.Drawing.Image.FromStream(file);

                        if (img.Width > img.Height)
                        {
                            Lampiran.Width = 300;
                            Lampiran.Height = 200;
                        }
                        else if (img.Width < img.Height)
                        {
                            Lampiran.Width = 180;
                            Lampiran.Height = 225;
                        }
                        else
                        {
                            Lampiran.Width = 200;
                            Lampiran.Height = 200;
                        }
                    }
                    else if (extention == ".pdf")
                    {
                        Lampiran.ImageUrl = "~/Lampiran/PDF/PDF.png";
                        FileStream file = new FileStream(MapPath("~/Lampiran/PDF/PDF.png"), FileMode.Open, FileAccess.Read, FileShare.Read);
                        System.Drawing.Image img = System.Drawing.Image.FromStream(file);

                        if (img.Width > img.Height)
                        {
                            Lampiran.Width = 300;
                            Lampiran.Height = 200;
                        }
                        else if (img.Width < img.Height)
                        {
                            Lampiran.Width = 180;
                            Lampiran.Height = 225;
                        }
                        else
                        {
                            Lampiran.Width = 200;
                            Lampiran.Height = 200;
                        }

                        Lampiran.Enabled = false;
                        Labelsukses.Visible = true;
                        Labelsukses.Text = report.lampiranLaporan.ToString();
                        Button1.Visible = true;
                        GlobalVariable.Filename = report.lampiranLaporan.ToString();
                    }
                    else
                    {
                        Lampiran.ImageUrl = "~/Lampiran/Image/" + report.lampiranLaporan.ToString();

                        FileStream file = new FileStream(MapPath("~/Lampiran/Image/" + report.lampiranLaporan.ToString()), FileMode.Open, FileAccess.Read, FileShare.Read);
                        System.Drawing.Image img = System.Drawing.Image.FromStream(file);

                        if (img.Width > img.Height)
                        {
                            Lampiran.Width = 300;
                            Lampiran.Height = 200;
                        }
                        else if (img.Width < img.Height)
                        {
                            Lampiran.Width = 150;
                            Lampiran.Height = 225;
                        }
                        else
                        {
                            Lampiran.Width = 200;
                            Lampiran.Height = 200;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                LabelStatus.Visible = true;
                LabelStatus.Text = ex.Message;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string filename = GlobalVariable.Filename;
            Response.Redirect("~/Lampiran/PDF/" + filename);
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            GlobalVariable.purposeEmail = TextBoxEmail.Text.Trim();
            GlobalVariable.subjectEmail = TextBoxSubjeklaporan.Text.Trim();
            GlobalVariable.contentEmail = TextareaIsi.Value;
            Response.Redirect("Reply.aspx");
        }
    }
}