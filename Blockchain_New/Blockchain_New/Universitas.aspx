﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterpageCoba.Master" AutoEventWireup="true" CodeBehind="Universitas.aspx.cs" Inherits="Blockchain_New.Universitas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Add Instansi</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <link href="Universitasstyle.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link rel="Icon" href="Aset/favicon.png" type="image/png" /> 
    <script src="https://kit.fontawesome.com/16b5cf1a92.js" crossorigin:"anonymous"></script>
    <script type="text/javascript">
        function HideLabel()
        {
            var seconds = 5;
            setTimeout
                (function () {
                    document.getElementById("<%=Labelberhasil.ClientID %>").style.display = "none";
                }, seconds * 1000);
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <br/>
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="col" style="background-color:#2d3bac; font-size:15px;">
                <div class="helpdesk" style="padding-left:30px; color:white;">
                    <br />
                    Helpdesk
                    <br />
                    <br />
                    00-xxxx-11
                    <p>
                        mail@mail.com
                        <br />
                        <br />
                        <br />
                        <br />

                    </p>
                </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="add" style=" padding-left:30px; color:black; border:4px solid #2d3bac">

                <h3><strong>Add Instansi</strong></h3>
                <br />
                <div class="form-group">
                    Nama Instansi
                    <p style="padding-bottom:2px !important">
                        <asp:TextBox runat="server" ID="TextBoxNamauniversitas" style="text-transform:uppercase; width:200px"></asp:TextBox>
                        <asp:Button ID="ButtonAdd" CssClass="myadd" runat="server" Text="Add" OnClick="ButtonAdd_Click"/>
                    </p>
                    <p>
                        <asp:Label ID="Labelberhasil" runat="server" CssClass="label" style="color:red; font-size:12px"></asp:Label>
                    </p>
                </div>
            </div>
          </div>
        </div>
      </div>


    <br />

      <div class="container">
        <div class="row">
          <div class="col-md-2">
          </div>

          <div class="col-md-8">
            <div class="col" style="width:100%; ">
                <asp:GridView ID="Data" CssClass="datauniversitas" runat="server" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:BoundField DataField="instansiName" HeaderText="List Instansi" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" CssClass="mydetail" ID="Deleteuniv" Text="Delete" OnClick = "Delete_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                </asp:GridView>
            <br />
          </div>

          <div class="col-md-2">
          </div>
          </div>
        </div>
      </div>
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />

</asp:Content>
