import pandas as pd
from sqlalchemy import create_engine
import hashlib
import json
from datetime import datetime
import sys
import psycopg2
from flask import json, Response, Flask, request
app = Flask(__name__)
app.config["DEBUG"] = True

fee = float(10)
#menyimpan data transaksi sementara
open_data = []

# membuat block genesis
genesis_block = {'Index': 0,
                 'Nonce': 30,
                 'Waktu': str(datetime.now()),
                 'Previous Hash': '0000000000000000000000000000000000000000000000000000000000000000',
                 'Data Penyimpanan': []
                 }

blockchain = [genesis_block]

# Melakukan proses hashing


def hashing(block):
  return hashlib.sha256(json.dumps(block).encode()).hexdigest()

# Memvalidasi hash data


def valid_proof(input_data, last_hash, nonce):
  exp = (str(input_data) + str(last_hash) + str(nonce)).encode()
  guess_hash = hashlib.sha256(exp).hexdigest()
  return guess_hash[0:2] == '30'

# Membuat nonce dari suatu proses hashing


def pow_nonce():
  last_block = blockchain[-1]
  last_hash = hashing(last_block)
  nonce = 0
  while not valid_proof(open_data, last_hash, nonce):
    nonce += 1
  return nonce

# Memperoleh block terakhir dalam blockchain


def get_last_value():
  return blockchain[-1]

# Menambah data


def add_data(pengirim, in_data):
  transaction = {'Pemilik': pengirim,
                 'Penyimpan': 'MK', 'Data Ijazah': in_data}
  open_data.append(transaction)

# Melakukan proses mining


def mine_block():
  last_block = blockchain[-1]
  hashed_block = hashing(last_block)
  nonce = pow_nonce()
  reward_transaction = {'Pengirim': 'MK', 'Penerima': 'Miners', 'Jumlah': fee}
  open_data.insert(0, reward_transaction)
  block = {'Index': len(blockchain),
           'Nonce': nonce,
           'Waktu': str(datetime.now()),
           'Previous Hash': hashed_block,
           'Data Penyimpanan': open_data
           }
  blockchain.append(block)


def add_data_data(id_data, email, gelar1, nama, gelar2, ttl, instansi, nomor, tanggal, tahun):
  tx_data = get_data(gelar1, nama, gelar2, ttl, instansi, nomor, tanggal, tahun)
  add_data(email, tx_data)

  mine_block()

  try:
    connection = psycopg2.connect(host="45.13.132.196", port=5432,
                              database="blockchain_dev", user="userpg2", password="userpg2")
    cursor2 = connection.cursor()
    query2 = 'insert into blockchain (id_data, nonce, timestamp, email, previous_hash) values (%s, %s, %s, %s, %s)'
    cursor2.execute(query2, (id_data, blockchain[-1]['Nonce'], blockchain[-1]
                             ['Waktu'], email, blockchain[-1]['Previous Hash']))
    connection.commit()
    cursor2.close()
    connection.close()
  except Exception as e:
    print(e)

# Memperoleh hasil dari input


def get_data(gelar1, nama, gelar2, ttl, instansi, nomor, tanggal, tahun):
  return gelar1, nama, gelar2, ttl, instansi, nomor, tanggal, tahun

def get_data_kelulusan():
  conn = psycopg2.connect(host = "45.13.132.196", port = 5432, database = "edu2019", user = "postgres", password = "edu2019jakarta")
  
  sql_query = "SELECT users.name, test_participations.name as test_name, test_participations.score, test_participations.date as test_date, tests.semester, tests.seq as sks, users.mailaddr as email\
   FROM users, test_participations, tests\
   WHERE users.mailaddr = test_participations.mailaddr AND tests.name = test_participations.name AND test_participations.passed = true ORDER BY 1, 2 ASC"
   
  table_kelulusan = pd.read_sql_query(sql_query, conn)
  cursor = conn.cursor()
  return table_kelulusan

def insert_data():
  engine = create_engine('postgresql+psycopg2://postgres:edu2019jakarta@45.13.132.196:5432/blockchain_dev')
  table = get_data_kelulusan()
  table.to_sql('kelulusan', con = engine, if_exists='replace', index=False)

@app.route('/backupdata', methods=['GET'])
def bcupdata():
  get_data_kelulusan()
  insert_data()
  return Response("success", status=200)

@app.route('/hello', methods=['POST'])
def main():
  # id_data = str(sys.argv[1])
  id_data = request.get_json()['idData']
  # username = str(sys.argv[2])
  email = request.get_json()['email']
  # gelar1 = str(sys.argv[3])
  gelar1 = request.get_json()['gelarDepan']
  # nama = str(sys.argv[4])
  nama = request.get_json()['nama']
  # gelar2 = str(sys.argv[5])
  gelar2 = request.get_json()['gelarBelakang']
  # ttl = str(sys.argv[6])
  ttl = request.get_json()['ttl']
  # instansi = str(sys.argv[7])
  instansi = request.get_json()['instansi']
  # nomor = str(sys.argv[8])
  nomor = request.get_json()['noIjazah']
  # tanggal = str(sys.argv[9])
  tanggal = request.get_json()['tglIjazah']
  # tahun = int(sys.argv[10])
  tahun = request.get_json()['thnIjazah']
  add_data_data(id_data, email, gelar1, nama, gelar2,ttl, instansi, nomor, tanggal, tahun)
  print(blockchain)
  return Response(json.dumps(blockchain), status=200)
  # return Response("success",status=200)


if __name__ == '__main__':
    app.run(host='0.0.0.0')