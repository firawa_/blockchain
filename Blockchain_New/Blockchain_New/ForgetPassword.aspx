﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ForgetPassword.aspx.cs" Inherits="Blockchain_New.ForgetPassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Forget Password</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <link href="Accountsettingstyle.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link rel="Icon" href="Aset/favicon.png" type="image/png" /> 
    <script src="https://kit.fontawesome.com/16b5cf1a92.js" crossorigin:"anonymous"></script>
    <script type="text/javascript">
        function HideLabel() {
            var seconds = 5;
            setTimeout
                (function () {
                    document.getElementById("<%=LabelBerhasil.ClientID %>").style.display = "none";
                }, seconds * 1000);
        };
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <header id="header" style="background-color: #0d1463; color:#ffffff; padding-bottom: 10px; padding-left:5px;">
            <div class="row">
              <div class="col-md-10" style="font-size:40px !important; padding-top:18px; padding-bottom:10px;">
<div style="align: justify; color:white; width:100%; font-size:23px">
<a href="Login.aspx"><img height="100" src="Aset/Logo.jpeg" style="float: left; margin-right:9px; height:60px" width="80"/></a>Blockchain based <br />Certificate Management</div>
              </div>
            </div>
        </header>
      <div class="container">
        <div class="row">
          <div class="col-md-2">

          </div>

          <div class="col-md-8">
            <h3>Let Us Help You</h3>
            <br />
            <div class="col-25">
                <label for="enter">Enter your E-mail</label>
            </div>
            <div class="col-75">
                <asp:TextBox Class="form-control" ID="TextBoxEnter" runat="server"  required></asp:TextBox>
            </div>
            <br />
            <div class="row">
                <asp:Button class="ubah" ID="ButtonSearch" runat="server" Text="Submit" OnClick ="ButtonSearch_Click"/>
                <asp:Label ID="LabelBerhasil" runat="server" style="margin-left:10px; color:red; font-size:13px;"></asp:Label>
            </div>
            <br />
          </div>
          </div>

          <div class="col-md-2">

          </div>
        </div>
      </div>

    </form>
</body>
</html>
