﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Expired.aspx.cs" Inherits="Blockchain_New.Expired" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Notification</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <link href="Accountsettingstyle.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link rel="Icon" href="Aset/favicon.png" type="image/png" /> 
    <script src="https://kit.fontawesome.com/16b5cf1a92.js" crossorigin:"anonymous"></script>
    <script type="text/javascript">
        function HideLabel()
        {
            var seconds = 5;
            setTimeout
                (function () {
                    document.getElementById("<%=Label1.ClientID %>").style.display = "none";
                }, seconds * 1000);
        };
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <header id="header" style="background-color: #0d1463; color:#ffffff; padding-bottom: 10px; padding-left:5px;">
            <div class="row">
              <div class="col-md-10" style="font-size:40px !important; padding-top:18px; padding-bottom:10px;">
<div style="align: justify; color:white; width:100%; font-size:23px">
<a href="Login.aspx"><img height="100" src="Aset/Logo.jpeg" style="float: left; margin-right:9px; height:60px" width="80"/></a>Blockchain based <br />Certificate Management</div>
              </div>
            </div>
        </header>
      <div class="container">
        <div class="row">
          <div class="col-md-1">

          </div>

          <div class="col-md-10"><br /><br />
            <h3 style="font-weight:bold">Sorry, Your Token Has Expired !</h3>
            <h4>We will send it again, just fill the form below with your e-mail</h4>
            <br />
            <div class="col-25">
                <label for="enter">E-mail</label>
            </div>
            <div class="col-75">
                <asp:TextBox Class="form-control" ID="TextBoxEnter" runat="server"  required></asp:TextBox>
            </div>
            <br />
            <div class="row">
                <asp:Button class="ubah2" ID="ButtonSearch" runat="server" Text="Resend Email" OnClick="ButtonSearch_Click"/>
                <p style="padding-top:0.5%"><asp:Label ID="Label1" runat="server" Text="Label" Visible="false" style="padding-left:15%"></asp:Label></p>
            </div>
            <br />
              
          </div>
          </div>
          
          <div class="col-md-1">

          </div>
        </div>
      </div>

    </form>
</body>
</html>
