﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Blockchain_New.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login Page</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="Loginstyle.css" rel="stylesheet" />
    <link rel="Icon" href="Aset/favicon.png" type="image/png" /> 
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"/>
    <script type="text/javascript">
        function HideLabel()
        {
            var seconds = 5;
            setTimeout
                (function () {
                    document.getElementById("<%=Label1Login.ClientID %>").style.display = "none";
                }, seconds * 1000);
        };
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <nav class="navbar navbar-default">
      <div class="container" style="width: 98%!important">
        <div class="navbar-header"> 
<div style="align: justify; color:white; width:300px">
<img height="100" src="Aset/Logo.jpeg" style="float: left; margin-right:9px; height:60px" width="80" /><p style="padding-top:6.8%; margin-bottom:0px!important">Blockchain based <br />Certificate Management</p></div>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="FAQ.aspx">FAQ</a></li>
            <li><a href="ContactUs.aspx">Contact Us</a></li>
            <li><a href="Login.aspx">Login/Register</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

      <div class="container">
        <div class="row">
          <div class="col-md-6">
          </div>

          <div class="col-md-6">
            <div class="panel panel-default">
                <center>Login for Blockchain Account<br/>Fill in your E-mail and Password, and click the Login button</center>
            </div>
          </div>
        </div>
      </div>



      <div class="containerr">
        <div class="row">
          <div class="col-md-8">
          </div>

          <div class="col-md-4">
            <div class="panel panel-default">
            </div>

            <div class="panel panel-default">
              <div class="panel-body">
                  <div class="back">
                      <div class="dalam">
                      <br />
                        <img class="card-img-top" src="Aset/orang.png" alt="Card image cap"/>
                        <br />
                        <br />

                        <div class="row">
                          <div class="col-25">
                            <label for="fname">E-mail</label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-75">
                            <asp:TextBox CssClass="form-control" ID="TextBoxUsername" runat="server" required MaxLength="40"></asp:TextBox>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-25">
                            <label for="fname">Password</label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-75">
                            <asp:TextBox CssClass="form-control" ID="TextBoxPassword" runat="server" TextMode="Password" required></asp:TextBox>
                            <p style="text-align:right; margin-bottom:0px!important"><a href="ForgetPassword.aspx">Forgot Password?</a></p>
                          </div>
                        </div>
                                <p>
                                    <asp:Label ID="Label1Login" runat="server"></asp:Label>
                                </p>

                        <br />
                        <div class="row">
							<asp:Button CssClass="btn btn-primary" style="margin-left:5%" ID="ButtonLogin" runat="server" Text="Login" OnClick="ButtonLogin_Click" />
							<a href="Register.aspx" class="card-link" style="margin-left:50%">Register</a>
                        </div>
                        <br />
                      </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    </form>
</body>
</html>
