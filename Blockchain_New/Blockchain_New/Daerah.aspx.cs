﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;
using Blockchain_New.Models;
using System.Net.Http.Formatting;
using System.Collections.Generic;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Web.UI.WebControls;

namespace Blockchain_New
{
    public partial class Daerah : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["email"] == null && Session["role"] == null)
            {
                logout();
            }
            else if (Session["role"].ToString() == "USER")
            {
                Response.Redirect("Home.aspx");
            }
            else if (Session["role"].ToString() == "SUPERADMIN")
            {
                Response.Redirect("Home3.aspx");
            }
            else if (Session["role"].ToString() == "INSTANSI")
            {
                Response.Redirect("Home4.aspx");
            }
            else if (Session["role"].ToString() == "PERUSAHAAN")
            {
                Response.Redirect("Homeperusahaaan.aspx");
            }
            else
            {
                if (!Page.IsPostBack)
                {
                    LoadMyDropDown();
                    LoadMyProvice();
                }   
            }    
        }

        public void logout()
        {
            string email = GlobalVariable.email;
            try
            {
                using (var client = new HttpClient())
                {
                    Users logout = new Users
                    {
                        email = email
                    };

                    client.BaseAddress = new Uri("http://localhost:9999/users/");
                    var response = client.PostAsJsonAsync("logout", logout).Result;
                    var content = response.Content.ReadAsStringAsync().Result;

                    if (response.IsSuccessStatusCode)
                    {
                        Session.Abandon();
                        Response.Redirect("Login.aspx");
                    }

                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void ButtonAdd2_Click(object sender, EventArgs e)
        {
            try
            {
                string email = Session["email"].ToString();

                if (email != null)
                {
                    using (var client = new HttpClient())
                    {
                        Provinsi province = new Provinsi
                        {
                            provinceName = TextBoxNamaProvinsi.Text.Trim().ToUpper()
                        };

                        client.BaseAddress = new Uri("http://localhost:9999/provinsi/");
                        var response = client.PostAsJsonAsync("tambahProvinsi", province).Result;
                        var content = response.Content.ReadAsStringAsync().Result;

                        if (response.IsSuccessStatusCode)
                        {
                            TextBoxNamaProvinsi.Text = string.Empty;

                            Labelberhasil2.Visible = true;
                            Labelberhasil2.ForeColor = System.Drawing.Color.Green;
                            Labelberhasil2.Text = content;
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel2();", true);
                        }
                        else
                        {
                            Labelberhasil2.Visible = true;
                            Labelberhasil2.Text = "Province name already exist.";
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel2();", true);
                        }
                    }
                }
                AfterAddProvince();
                LoadMyProvice();
                LoadMyDropDown();
            }
            catch (Exception ex)
            {
                Labelberhasil2.Text = ex.Message;
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel2();", true);
            }
        }

        protected void ButtonAdd3_Click(object sender, EventArgs e)
        {
            try
            {
                string email = Session["email"].ToString();

                if (email != null)
                {
                    using (var client = new HttpClient())
                    {
                        Kota city = new Kota
                        {
                            provinceId = int.Parse(DropDownListProv.SelectedItem.Value),
                            cityName = TextBoxKabkota.Text.Trim().ToUpper()
                        };

                        client.BaseAddress = new Uri("http://localhost:9999/kota/");
                        var response = client.PostAsJsonAsync("tambahKota", city).Result;
                        var content = response.Content.ReadAsStringAsync().Result;

                        if (response.IsSuccessStatusCode)
                        {
                            TextBoxKabkota.Text = string.Empty;

                            Labelberhasil3.Visible = true;
                            Labelberhasil3.ForeColor = System.Drawing.Color.Green;
                            Labelberhasil3.Text = content;
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel3();", true);
                        }
                        else
                        {
                            Labelberhasil3.Visible = true;
                            Labelberhasil3.Text = "City name already exist.";
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel3();", true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Labelberhasil3.Text = ex.Message;
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel3();", true);
            }
            AfterAddProvince();
            LoadMyProvice();
            LoadMyDropDown();
            Refresh();
        }

        public void LoadMyProvice()
        {
            string email = Session["email"].ToString();

            if (email != null)
            {
                try
                {
                    HttpClient clients = new HttpClient();
                    clients.BaseAddress = new Uri("http://localhost:9999/provinsi/");
                    clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage rs = clients.GetAsync("daftarProvinsi").Result;
                    if (rs.IsSuccessStatusCode)
                    {
                        var content = rs.Content.ReadAsStringAsync().Result;
                        ListProvinsi.DataSource = (new JavaScriptSerializer()).Deserialize<List<Provinsi>>(content);
                        ListProvinsi.DataBind();
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }

        public void LoadMyCity()
        {
            string province_id = GlobalVariable.proviceid;
            string email = Session["email"].ToString();

            if (email != null)
            {
                try
                {
                    HttpClient clients = new HttpClient();
                    clients.BaseAddress = new Uri("http://localhost:9999/kota/");
                    clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage rs = clients.GetAsync("daftarKotaByProvince?provinceId=" + province_id + "").Result;
                    if (rs.IsSuccessStatusCode)
                    {
                        var content = rs.Content.ReadAsStringAsync().Result;
                        ListDaerah.DataSource = (new JavaScriptSerializer()).Deserialize<List<Kota>>(content);
                        ListDaerah.DataBind();
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }

        public void LoadMyDropDown()
        {
            string email = Session["email"].ToString();

            if (email != null)
            {
                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri("http://localhost:9999/provinsi/");
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = clients.GetAsync("daftarProvinsi").Result;
                
                if (response.IsSuccessStatusCode)
                {
                    var content = response.Content.ReadAsStringAsync().Result;
                    List<Provinsi> provinsiList = (new JavaScriptSerializer()).Deserialize<List<Provinsi>>(content);
                    DropDownListProv.DataSource = provinsiList;
                    DropDownListProv.DataTextField = "provinceName";
                    DropDownListProv.DataValueField = "provinceId";
                    DropDownListProv.DataBind();
                }
            }
        }

        public void Refresh()
        {
            Labelberhasil2.Text = string.Empty;
            DropDownListProv.SelectedIndex = 0;
        }

        public void AfterAddProvince()
        {
            DropDownListProv.Items.Clear();
            DropDownListProv.Items.Add("--Pilih Provinsi--");
            DropDownListProv.SelectedIndex = 0;
        }

        protected void Delete_Provinsi_Click(object Sender, EventArgs e)
        {
            LinkButton detailbutton = Sender as LinkButton;

            GridViewRow record = detailbutton.NamingContainer as GridViewRow;

            string province_name = record.Cells[1].Text;

            try
            {
                using (var client = new HttpClient())
                {
                    Provinsi province = new Provinsi
                    {
                        provinceName = province_name
                    };

                    client.BaseAddress = new Uri("http://localhost:9999/provinsi/");
                    var response = client.PostAsJsonAsync("hapusProvinsi", province).Result;
                    var content = response.Content.ReadAsStringAsync().Result;

                    if (response.IsSuccessStatusCode)
                    {
                        LoadMyProvice();
                        LoadMyCity();
                        Labelberhasil2.Visible = true;
                        Labelberhasil2.Text = "Success to delete province.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel2();", true);
                    }
                    else
                    {
                        Labelberhasil2.Text = "Error";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel2();", true);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void Delete_Kota_Click(object Sender, EventArgs e)
        {
            LinkButton detailbutton = Sender as LinkButton;

            GridViewRow record = detailbutton.NamingContainer as GridViewRow;

            string city_name = record.Cells[0].Text;

            try
            {
                using (var client = new HttpClient())
                {
                    Kota city = new Kota
                    {
                        cityName = city_name
                    };

                    client.BaseAddress = new Uri("http://localhost:9999/kota/");
                    var response = client.PostAsJsonAsync("hapusKota", city).Result;
                    var content = response.Content.ReadAsStringAsync().Result;

                    if (response.IsSuccessStatusCode)
                    {
                        LoadMyCity();
                        Labelberhasil3.Visible = true;
                        Labelberhasil3.Text = "Success to delete city.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel3();", true);
                    }
                    else
                    {
                        Labelberhasil3.Text = "Error";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel3();", true);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void Show_City_Click(object Sender, EventArgs e)
        {
            LinkButton detailbutton = Sender as LinkButton;

            GridViewRow record = detailbutton.NamingContainer as GridViewRow;

            string province_id = record.Cells[0].Text;

            GlobalVariable.proviceid = province_id;

            string email = Session["email"].ToString();

            if (email != null)
            {
                try
                {
                    HttpClient clients = new HttpClient();
                    clients.BaseAddress = new Uri("http://localhost:9999/kota/");
                    clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage rs = clients.GetAsync("daftarKotaByProvince?provinceId=" + province_id + "").Result;
                    if (rs.IsSuccessStatusCode)
                    {
                        var content = rs.Content.ReadAsStringAsync().Result;
                        ListDaerah.DataSource = (new JavaScriptSerializer()).Deserialize<List<Kota>>(content);
                        ListDaerah.DataBind();
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }
    }
}