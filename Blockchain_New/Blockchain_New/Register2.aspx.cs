﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;
using Blockchain_New.Models;
using System.Net.Http.Formatting;
using System.Collections.Generic;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

namespace Blockchain_New
{
    public partial class Register2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        public void DoRegister()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Users user = new Users
                    {
                        email = TextBoxUsername.Text.Trim(),
                        password = TextBoxPassword.Text,
                        channel = 999999,
                        name = TextBoxInstansi.Text.ToUpper(),
                        activated = false
                    };

                    var hasNumber = new Regex(@"[0-9]+");
                    var hasUpperChar = new Regex(@"[A-Z]+");
                    var hasMiniMaxChars = new Regex(@".{8,32}");
                    var hasLowerChar = new Regex(@"[a-z]+");
                    var hasSymbols = new Regex(@"[!@#$%^&*()_+=\[{\]};:<>|./?,-]");
                    var email = new Regex(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");

                    var pass = TextBoxPassword.Text;
                    var uname = TextBoxUsername.Text;

                    if (!hasLowerChar.IsMatch(pass))
                    {
                        Label1Register.Visible = true;
                        Label1Register.ForeColor = System.Drawing.Color.Red;
                        Label1Register.Text = "Your password should contain at least one lower case letter.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else if (!hasUpperChar.IsMatch(pass))
                    {
                        Label1Register.Visible = true;
                        Label1Register.ForeColor = System.Drawing.Color.Red;
                        Label1Register.Text = "Your password should contain at least one upper case letter.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else if (!hasMiniMaxChars.IsMatch(pass))
                    {
                        Label1Register.Visible = true;
                        Label1Register.ForeColor = System.Drawing.Color.Red;
                        Label1Register.Text = "Your password should not be less than 8 characters.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else if (!hasNumber.IsMatch(pass))
                    {
                        Label1Register.Visible = true;
                        Label1Register.ForeColor = System.Drawing.Color.Red;
                        Label1Register.Text = "Your password should contain at least one numeric value.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else if (!hasSymbols.IsMatch(pass))
                    {
                        Label1Register.Visible = true;
                        Label1Register.ForeColor = System.Drawing.Color.Red;
                        Label1Register.Text = "Your password should contain at least one special case characters.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else if (!email.IsMatch(uname))
                    {
                        Label1Register.Visible = true;
                        Label1Register.ForeColor = System.Drawing.Color.Red;
                        Label1Register.Text = "Your email is not valid.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else if (TextBoxUsername.Text.Trim() == TextBoxPassword.Text.Trim())
                    {
                        Label1Register.Visible = true;
                        Label1Register.ForeColor = System.Drawing.Color.Red;
                        Label1Register.Text = "Your password cannot be the same as your username.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else
                    {
                        client.BaseAddress = new Uri("http://localhost:9999/users/");
                        var response = client.PostAsJsonAsync("registerInstansi", user).Result;
                        var content = response.Content.ReadAsStringAsync().Result;

                        if (response.IsSuccessStatusCode)
                        {
                            sendemail();

                            TextBoxUsername.Text = string.Empty;
                            TextBoxInstansi.Text = string.Empty;
                            TextBoxPassword.Text = string.Empty;

                            Label1Register.Visible = true;
                            Label1Register.ForeColor = System.Drawing.Color.Green;
                            Label1Register.Text = "Instansi is successful to register.";
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                            Response.Redirect("Himbauan.aspx?status=1");
                        }
                        else if (content == "Instansi name already registered!")
                        {
                            Label1Register.Visible = true;
                            Label1Register.ForeColor = System.Drawing.Color.Red;
                            Label1Register.Text = "Instansi name has been used.";
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                        }
                        else
                        {
                            Label1Register.Visible = true;
                            Label1Register.ForeColor = System.Drawing.Color.Red;
                            Label1Register.Text = "Email address has been used.";
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Label1Register.Visible = true;
                Label1Register.ForeColor = System.Drawing.Color.Red;
                Label1Register.Text = ex.Message;
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
            }
        }

        protected void ButtonRegister_Click1(object sender, EventArgs e)
        {
            GlobalVariable.NameUser = TextBoxInstansi.Text;
            GlobalVariable.email = TextBoxUsername.Text;
            DoRegister();
        }

        public void sendemail()
        {
            string token = getToken();
            string name = TextBoxInstansi.Text;
            if (token != "Failed")
            {
                //string contents = "<html><head runat=\"server\"><title>Activision Account</title><meta charset=\"utf-8\"/><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\"/></head><body><div style=\"width:60%; margin: 0 auto; font-weight:none;\"><br /><br /><div style=\"padding-left:10%; font-size:19px\">Hello " + name + ",<br /><br /><p>Welcome to Blockchain based Certificate Management!</p><p>To activate your account please click the button below to verify your email address:</p></div><br /><center><a style=\"background-color:#6495ED; color:white; padding:2% 4%;\" href=http://localhost:50252/Aktivasi.aspx?token=" + token + ">Activate Account</a><br/><p>This link will expire in 24 hours. Please use this link soon.</p><br/><div style=\"font-size:11px; padding-top:1%;\"></center><br /><div style=\"padding-left:10%; font-size:15px\"></div><br /><br /><div style=\"padding-left:10%; font-size:15px\">Thank you,<br /><br /><br />Blockchain based Certificate Management Team</div></div></body></html>";
                string contents = "<html><head runat=\"server\"><title>Activation Account</title><meta charset=\"utf-8\"/><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\"/></head><body><div style=\"width:60%; margin: 0 auto; font-weight:none;\"><br /><br /><div style=\"padding-left:10%; font-size:19px\">Hello " + name + ",<br /><br /><p>Welcome to Blockchain based Certificate Management!</p><p>To activate your account please click the button below to verify your email address:</p></div><br /><center><a style=\"background-color:#6495ED; color:white; padding:2% 4%;\" href=https://banksertifikat.com/Aktivasi.aspx?token=" + token + ">Activate Account</a><br/><p>This link will expire in 24 hours. Please use this link soon.</p><br/><div style=\"font-size:11px; padding-top:1%;\"></center><br /><div style=\"padding-left:10%; font-size:15px\"></div><br /><br /><div style=\"padding-left:10%; font-size:15px\">Thank you,<br /><br /><br />Blockchain based Certificate Management Team</div></div></body></html>";
                try
                {
                    using (var client = new HttpClient())
                    {
                        Email mailaddres = new Email
                        {
                            mailTo = TextBoxUsername.Text,
                            mailSubject = "Activation Email",
                            mailContent = contents
                        };

                        client.BaseAddress = new Uri("http://localhost:9999/mail/");
                        var response = client.PostAsJsonAsync("sendMail", mailaddres).Result;
                        var content = response.Content.ReadAsStringAsync().Result;

                        if (response.IsSuccessStatusCode)
                        {
                            Label1Register.Visible = true;
                            Label1Register.Text = "Sukses";
                        }
                        else
                        {
                            Label1Register.Visible = true;
                            Label1Register.Text = "fail";
                        }
                    }
                }
                catch (Exception ex)
                {
                    Label1Register.Visible = true;
                    Label1Register.Text = ex.Message;
                }
            }
        }

        public string getToken()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Users email = new Users
                    {
                        email = TextBoxUsername.Text
                    };

                    client.BaseAddress = new Uri("http://localhost:9999/users/");
                    var response = client.PostAsJsonAsync("getActivationToken", email).Result;
                    var content = response.Content.ReadAsStringAsync().Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string token = content;
                        return token;
                    }
                    else
                    {
                        return "Failed";
                    }
                }
            }
            catch (Exception ex)
            {
                Label1Register.Visible = true;
                Label1Register.Text = ex.Message;
                return "Failed";
            }
        }
    }
}