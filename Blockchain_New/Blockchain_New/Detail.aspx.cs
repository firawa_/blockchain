﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;
using Blockchain_New.Models;
using System.Net.Http.Formatting;
using System.Collections.Generic;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Web.UI.WebControls;
using System.IO;

namespace Blockchain_New
{
    public partial class Detail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["email"] == null && Session["role"] == null)
            {
                logout();
            }
            else if (Session["role"].ToString() == "ADMIN")
            {
                Response.Redirect("Home2.aspx");
            }
            else if (Session["role"].ToString() == "SUPERADMIN")
            {
                Response.Redirect("Home3.aspx");
            }
            else if (Session["role"].ToString() == "INSTANSI")
            {
                Response.Redirect("Home4.aspx");
            }
            else if (Session["role"].ToString() == "PERUSAHAAN")
            {
                Response.Redirect("Homeperusahaan.aspx");
            }
            else
            {
                GetData();
            }    
        }

        public void logout()
        {
            string email = GlobalVariable.email;
            try
            {
                using (var client = new HttpClient())
                {
                    Users logout = new Users
                    {
                        email = email
                    };

                    client.BaseAddress = new Uri("http://localhost:9999/users/");
                    var response = client.PostAsJsonAsync("logout", logout).Result;
                    var content = response.Content.ReadAsStringAsync().Result;

                    if (response.IsSuccessStatusCode)
                    {
                        Session.Abandon();
                        Response.Redirect("Login.aspx");
                    }

                }
            }
            catch (Exception ex)
            {

            }
        }

        public void GetData()
        {
            string id = Session["id"].ToString();
            try
            {
                string uri = "http://localhost:9999/";
                string parameter = "userdata/cariUserData?idData=" + id + "";
                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri(uri);
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = clients.GetAsync(parameter).Result;

                if (response.IsSuccessStatusCode)
                {
                    var content = response.Content.ReadAsStringAsync().Result;
                    var userdata = JsonConvert.DeserializeObject<UserData>(content);

                    TextBoxGelardepan.Text = userdata.gelarDepan.ToString();
                    TextBoxGelarbelakang.Text = userdata.gelarBelakang.ToString();
                    TextBoxNamalengkap.Text = userdata.nama.ToString();
                    TextBoxTTL.Text = userdata.ttl.ToString();
                    TextBoxUniversitas.Text = userdata.instansi.ToString();
                    TextBoxIjazah.Text = userdata.noIjazah.ToString();
                    TextBoxTanggaldikeluarkanijazah.Text = userdata.tglIjazah.ToString("dd/MM/yyy");
                    TextBoxTahunterbitijazah.Text = userdata.thnIjazah.ToString();
                  
                    string extention = System.IO.Path.GetExtension(userdata.scanFile.ToString());
                    if (extention == ".pdf")
                    {
                        ImageScanIjazah.ImageUrl = "~/Upload/PDF/PDF.png";
                        FileStream file = new FileStream(MapPath("~/Upload/PDF/PDF.png"), FileMode.Open, FileAccess.Read, FileShare.Read);
                        System.Drawing.Image img = System.Drawing.Image.FromStream(file);

                        if (img.Width > img.Height)
                        {
                            ImageScanIjazah.Width = 300;
                            ImageScanIjazah.Height = 200;
                        }
                        else if (img.Width < img.Height)
                        {
                            ImageScanIjazah.Width = 150;
                            ImageScanIjazah.Height = 225;
                        }
                        else
                        {
                            ImageScanIjazah.Width = 200;
                            ImageScanIjazah.Height = 200;
                        }

                        ImageScanIjazah.Enabled = false;
                        Labelsukses.Visible = true;
                        Labelsukses.Text = userdata.scanFile.ToString();
                        Button1.Visible = true;
                        GlobalVariable.Filename = userdata.scanFile.ToString();
                    }
                    else
                    {
                        ImageScanIjazah.ImageUrl = "~/Upload/Image/" + userdata.scanFile.ToString();
                        FileStream file = new FileStream(MapPath("~/Upload/Image/" + userdata.scanFile.ToString()), FileMode.Open, FileAccess.Read, FileShare.Read);
                        System.Drawing.Image img = System.Drawing.Image.FromStream(file);

                        if (img.Width > img.Height)
                        {
                            ImageScanIjazah.Width = 300;
                            ImageScanIjazah.Height = 200;
                        }
                        else if (img.Width < img.Height)
                        {
                            ImageScanIjazah.Width = 150;
                            ImageScanIjazah.Height = 225;
                        }
                        else
                        {
                            ImageScanIjazah.Width = 200;
                            ImageScanIjazah.Height = 200;
                        }
                    }
                }

            }
            catch (Exception ex)
            {

            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string filename = GlobalVariable.Filename;
            Response.Redirect("~/Upload/PDF/" + filename);
        }
    }
}