﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Himbauan.aspx.cs" Inherits="Blockchain_New.Himbauan" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Verify Email</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <link href="Accountsettingstyle.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link rel="Icon" href="Aset/favicon.png" type="image/png" /> 
    <script src="https://kit.fontawesome.com/16b5cf1a92.js" crossorigin:"anonymous"></script>
    <script type="text/javascript">
        function HideLabel()
        {
            var seconds = 5;
            setTimeout
                (function () {
                    document.getElementById("<%=Label2.ClientID %>").style.display = "none";
                }, seconds * 1000);
        };
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <header id="header" style="background-color: #0d1463; color:#ffffff; padding-bottom: 10px; padding-left:5px;">
            <div class="row">
              <div class="col-md-10" style="font-size:40px !important; padding-top:18px; padding-bottom:10px;">
<div style="align: justify; color:white; width:100%; font-size:23px">
<a href="Login.aspx"><img height="100" src="Aset/Logo.jpeg" style="float: left; margin-right:9px; height:60px" width="80"/></a>Blockchain based <br />Certificate Management</div>
              </div>
            </div>
        </header>
      <div class="container">
        <div class="row">
          <div class="col-md-2">

          </div>

          <div class="col-md-8">
            <center><br /><br /><br /><br />
            <div style="font-size:20px">
                Hello <asp:Label ID="Label1" runat="server"></asp:Label>,
                <br /><br />
                    Your account has been successfully registered. However, your email has not been verified.
                <br />
                Please check your email to verify your email and activate your account
            </div>
            <br />
            <div class="row">
                <br /><br />
                <div style="font-size:16px">Didn't get email verification? <asp:LinkButton ID="resendemail" runat="server" OnClick="resendemail_Click">Resend Email</asp:LinkButton></div>
                <br /><br />
                <h5>Thank you,
                <br /><br /><br />Blockchain based Certificate Management Team</h5>
            </div>
            <br />
          </div>
        </center></div>
          <center><asp:Label ID="Label2" runat="server" Text="Label" Visible="false" Style="color:red"></asp:Label></center>
          <div class="col-md-2">

          </div>
        </div>
    </form>
</body>
</html>
