﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;
using Blockchain_New.Models;
using System.Net.Http.Formatting;
using System.Collections.Generic;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

namespace Blockchain_New
{
    public partial class Create : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["email"] == null && Session["role"] == null)
            {
                logout();
            }
            else if (Session["role"].ToString() == "USER")
            {
                Response.Redirect("Home.aspx");
            }
            else if (Session["role"].ToString() == "ADMIN")
            {
                Response.Redirect("Home2.aspx");
            }
            else if (Session["role"].ToString() == "INSTANSI")
            {
                Response.Redirect("Home4.aspx");
            }
            else if (Session["role"].ToString() == "PERUSAHAAN")
            {
                Response.Redirect("Homeperusahaaan.aspx");
            }
        }
        public void logout()
        {
            string email = GlobalVariable.email;
            try
            {
                using (var client = new HttpClient())
                {
                    Users logout = new Users
                    {
                        email = email
                    };

                    client.BaseAddress = new Uri("http://localhost:9999/users/");
                    var response = client.PostAsJsonAsync("logout", logout).Result;
                    var content = response.Content.ReadAsStringAsync().Result;

                    if (response.IsSuccessStatusCode)
                    {
                        Session.Abandon();
                        Response.Redirect("Login.aspx");
                    }

                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void ButtonRegister_Click(object sender, EventArgs e)
        {
            DoRegisterAdmin();
        }

        public void DoRegisterAdmin()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Users admin = new Users
                    {
                        email = TextBoxUsername.Text.Trim(),
                        password = TextBoxPassword.Text,
                        channel = 999999,
                        name = TextBoxNamaadmin.Text.Trim().ToUpper(),
                        activated = false
                    };

                    var hasNumber = new Regex(@"[0-9]+");
                    var hasUpperChar = new Regex(@"[A-Z]+");
                    var hasMiniMaxChars = new Regex(@".{8,32}");
                    var hasLowerChar = new Regex(@"[a-z]+");
                    var hasSymbols = new Regex(@"[!@#$%^&*()_+=\[{\]};:<>|./?,-]");
                    var email = new Regex(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");

                    var pass = TextBoxPassword.Text;
                    var uname = TextBoxUsername.Text;

                    if (!hasLowerChar.IsMatch(pass))
                    {
                        LabelBerhasil.Visible = true;
                        LabelBerhasil.Text = "Your password should contain at least one lower case letter.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else if (!hasUpperChar.IsMatch(pass))
                    {
                        LabelBerhasil.Visible = true;
                        LabelBerhasil.Text = "Your password should contain at least one upper case letter.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else if (!hasMiniMaxChars.IsMatch(pass))
                    {
                        LabelBerhasil.Visible = true;
                        LabelBerhasil.Text = "Your password should not be less than 8 characters.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else if (!hasNumber.IsMatch(pass))
                    {
                        LabelBerhasil.Visible = true;
                        LabelBerhasil.Text = "Your password should contain at least one numeric value.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else if (!hasSymbols.IsMatch(pass))
                    {
                        LabelBerhasil.Visible = true;
                        LabelBerhasil.Text = "Your password should contain at least one special case characters.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else if (!email.IsMatch(uname))
                    {
                        LabelBerhasil.Visible = true;
                        LabelBerhasil.Text = "Your email is not valid.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else if (TextBoxUsername.Text.Trim() == TextBoxPassword.Text.Trim())
                    {
                        LabelBerhasil.Visible = true;
                        LabelBerhasil.Text = "Your password cannot be the same as your username.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else
                    {
                        client.BaseAddress = new Uri("http://localhost:9999/admin/");
                        var response = client.PostAsJsonAsync("register", admin).Result;
                        var s = response.Content.ReadAsStringAsync().Result;

                        if (response.IsSuccessStatusCode)
                        {
                            sendemail();
                            TextBoxUsername.Text = string.Empty;
                            TextBoxPassword.Text = string.Empty;
                            TextBoxNamaadmin.Text = string.Empty;

                            LabelBerhasil.Visible = true;
                            LabelBerhasil.ForeColor = System.Drawing.Color.Green;
                            LabelBerhasil.Text = "Admin is successful to register. Please Admin to check the e-mail for verification e-mail and activate account.";
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                        }
                        else
                        {
                            LabelBerhasil.Visible = true;
                            LabelBerhasil.Text = "Admin already exist.";
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                LabelBerhasil.Visible = true;
                LabelBerhasil.Text = ex.Message;
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
            }
        }

        public void sendemail()
        {
            string token = getToken();
            string name = TextBoxNamaadmin.Text;
            if (token != "Failed")
            {
                //string contents = "<html><head runat=\"server\"><title>Activision Account</title><meta charset=\"utf-8\"/><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\"/></head><body><div style=\"width:60%; margin: 0 auto; font-weight:none;\"><br /><br /><div style=\"padding-left:10%; font-size:19px\">Hello " + name + ",<br /><br /><p>Welcome to Blockchain based Certificate Management!</p><p>To activate your account please click the button below to verify your email address:</p></div><br /><center><a style=\"background-color:#6495ED; color:white; padding:2% 4%;\" href=http://localhost:50252/Aktivasi.aspx?token=" + token + ">Activate Account</a><br/><p>This link will expire in 24 hours. Please use this link soon.</p><br/><div style=\"font-size:11px; padding-top:1%;\"></center><br /><div style=\"padding-left:10%; font-size:15px\"></div><br /><br /><div style=\"padding-left:10%; font-size:15px\">Thank you,<br /><br /><br />Blockchain based Certificate Management Team</div></div></body></html>";
                string contents = "<html><head runat=\"server\"><title>Activision Account</title><meta charset=\"utf-8\"/><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\"/></head><body><div style=\"width:60%; margin: 0 auto; font-weight:none;\"><br /><br /><div style=\"padding-left:10%; font-size:19px\">Hello " + name + ",<br /><br /><p>Welcome to Blockchain based Certificate Management!</p><p>To activate your account please click the button below to verify your email address:</p></div><br /><center><a style=\"background-color:#6495ED; color:white; padding:2% 4%;\" href=https://banksertifikat.com/Aktivasi.aspx?token=" + token + ">Activate Account</a><br/><p>This link will expire in 24 hours. Please use this link soon.</p><br/><div style=\"font-size:11px; padding-top:1%;\"></center><br /><div style=\"padding-left:10%; font-size:15px\"></div><br /><br /><div style=\"padding-left:10%; font-size:15px\">Thank you,<br /><br /><br />Blockchain based Certificate Management Team</div></div></body></html>";
                try
                {
                    using (var client = new HttpClient())
                    {
                        Email mailaddres = new Email
                        {
                            mailTo = TextBoxUsername.Text,
                            mailSubject = "Activation Email",
                            mailContent = contents
                        };

                        client.BaseAddress = new Uri("http://localhost:9999/mail/");
                        var response = client.PostAsJsonAsync("sendMail", mailaddres).Result;
                        var content = response.Content.ReadAsStringAsync().Result;

                        if (response.IsSuccessStatusCode)
                        {
                            LabelBerhasil.Visible = true;
                            LabelBerhasil.Text = "Sukses";
                        }
                        else
                        {
                            LabelBerhasil.Visible = true;
                            LabelBerhasil.Text = "fail";
                        }
                    }
                }
                catch (Exception ex)
                {
                    LabelBerhasil.Visible = true;
                    LabelBerhasil.Text = ex.Message;
                }
            }
        }

        public string getToken()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Users email = new Users
                    {
                        email = TextBoxUsername.Text
                    };

                    client.BaseAddress = new Uri("http://localhost:9999/users/");
                    var response = client.PostAsJsonAsync("getActivationToken", email).Result;
                    var content = response.Content.ReadAsStringAsync().Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string token = content;
                        return token;
                    }
                    else
                    {
                        return "Failed";
                    }
                }
            }
            catch (Exception ex)
            {
                LabelBerhasil.Visible = true;
                LabelBerhasil.Text = ex.Message;
                return "Failed";
            }
        }
    }
}