﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;
using Blockchain_New.Models;
using System.Net.Http.Formatting;
using System.Collections.Generic;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Web.UI.WebControls;

namespace Blockchain_New
{
    public partial class Expired : System.Web.UI.Page
    {
        public string token { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            string status = Request.QueryString["status"];
        }

        public string getToken()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Users email = new Users
                    {
                        email = TextBoxEnter.Text
                    };

                    client.BaseAddress = new Uri("http://localhost:9999/users/");
                    var response = client.PostAsJsonAsync("getActivationToken", email).Result;
                    var content = response.Content.ReadAsStringAsync().Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string token = content;
                        return token;
                    }
                    else
                    {
                        return "Failed";
                    }
                }
            }
            catch (Exception ex)
            {
                Label1.Visible = true;
                Label1.ForeColor = System.Drawing.Color.Red;
                Label1.Text = ex.Message;
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                return "Failed";
            }
        }

        public string getName()
        {
            try
            {
                string email = TextBoxEnter.Text;
                string uri = "http://localhost:9999/";
                string parameter = "users/cariUsers?email=" + email + "";
                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri(uri);
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = clients.GetAsync(parameter).Result;

                if (response.IsSuccessStatusCode)
                {
                    var content = response.Content.ReadAsStringAsync().Result;
                    var userdata = JsonConvert.DeserializeObject<Users>(content);

                    string name = userdata.name;
                    return name;
                }
                else
                {
                    return "Failed404";
                }
            }
            catch (Exception ex)
            {
                Label1.Visible = true;
                Label1.Text = ex.Message;
                return "Failed404";
            }
        }

        public void sendemailuseractive()
        {
            string name = getName();

            //string contents = "<html><head runat=\"server\"><title>Account has been activated</title><meta charset=\"utf-8\"/><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\"/></head><body><div style=\"width:60%; margin: 0 auto; font-weight:none;\"><br /><br /><div style=\"padding-left:10%; font-size:19px\">Hello " + name + ",<br /><br /><p>Welcome to Blockchain based Certificate Management!</p><p>Your account has been activated. If you want to log into banksertifikat.com, click the link below</p></div><br /><center><a style=\"background-color:#6495ED; color:white; padding:2% 4%;\" href=http://localhost:50252/Login.aspx?>Login</a><br/><br/>Thank you,<br /><br /><br />Blockchain based Certificate Management Team</div></div></body></html>";
            string contents = "<html><head runat=\"server\"><title>Account has been activated</title><meta charset=\"utf-8\"/><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\"/></head><body><div style=\"width:60%; margin: 0 auto; font-weight:none;\"><br /><br /><div style=\"padding-left:10%; font-size:19px\">Hello " + name + ",<br /><br /><p>Welcome to Blockchain based Certificate Management!</p><p>Your account has been activated. If you want to log into banksertifikat.com, click the link below</p></div><br /><center><a style=\"background-color:#6495ED; color:white; padding:2% 4%;\" href=https://banksertifikat.com/Login.aspx?>Login</a><br/><br/>Thank you,<br /><br /><br />Blockchain based Certificate Management Team</div></div></body></html>";
            try
            {
                using (var client = new HttpClient())
                {
                    Email mailaddres = new Email
                    {
                        mailTo = TextBoxEnter.Text,
                        mailSubject = "Account has been activated",
                        mailContent = contents
                    };

                    client.BaseAddress = new Uri("http://localhost:9999/mail/");
                    var response = client.PostAsJsonAsync("sendMail", mailaddres).Result;
                    var content = response.Content.ReadAsStringAsync().Result;

                    if (response.IsSuccessStatusCode)
                    {
                        Label1.Visible = true;
                        Label1.ForeColor = System.Drawing.Color.Green;
                        Label1.Text = "Success to send email.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else
                    {
                        Label1.Visible = true;
                        Label1.ForeColor = System.Drawing.Color.Red;
                        Label1.Text = "Failed to send email.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                Label1.Visible = true;
                Label1.Text = ex.Message;
            }
        }

        public void sendemailuserinactive()
        {
            string token = getToken();
            string name = getName();
            if (token != "Failed" && name != "Failed404")
            {
                //string contents = "<html><head runat=\"server\"><title>Activision Account</title><meta charset=\"utf-8\"/><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\"/></head><body><div style=\"width:60%; margin: 0 auto; font-weight:none;\"><br /><br /><div style=\"padding-left:10%; font-size:19px\">Hello " + name + ",<br /><br /><p>Welcome to Blockchain based Certificate Management!</p><p>To activate your account please click the button below to verify your email address:</p></div><br /><center><a style=\"background-color:#6495ED; color:white; padding:2% 4%;\" href=http://localhost:50252/Aktivasi.aspx?token=" + token + ">Activate Account</a><br/><p>This link will expire in 24 hours. Please use this link soon.</p><br/><div style=\"font-size:11px; padding-top:1%;\"></center><br /><div style=\"padding-left:10%; font-size:15px\"></div><br /><br /><div style=\"padding-left:10%; font-size:15px\">Thank you,<br /><br /><br />Blockchain based Certificate Management Team</div></div></body></html>";

                string contents = "<html><head runat=\"server\"><title>Activation Account</title><meta charset=\"utf-8\"/><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\"/></head><body><div style=\"width:60%; margin: 0 auto; font-weight:none;\"><br /><br /><div style=\"padding-left:10%; font-size:19px\">Hello " + name + ",<br /><br /><p>Welcome to Blockchain based Certificate Management!</p><p>To activate your account please click the button below to verify your email address:</p></div><br /><center><a style=\"background-color:#6495ED; color:white; padding:2% 4%;\" href=https://banksertifikat.com/Aktivasi.aspx?token=" + token + ">Activate Account</a><br/><p>This link will expire in 24 hours. Please use this link soon.</p><br/><div style=\"font-size:11px; padding-top:1%;\"></center><br /><div style=\"padding-left:10%; font-size:15px\"></div><br /><br /><div style=\"padding-left:10%; font-size:15px\">Thank you,<br /><br /><br />Blockchain based Certificate Management Team</div></div></body></html>";
                try
                {
                    using (var client = new HttpClient())
                    {
                        Email mailaddres = new Email
                        {
                            mailTo = TextBoxEnter.Text,
                            mailSubject = "Activation Email",
                            mailContent = contents
                        };

                        client.BaseAddress = new Uri("http://localhost:9999/mail/");
                        var response = client.PostAsJsonAsync("sendMail", mailaddres).Result;
                        var content = response.Content.ReadAsStringAsync().Result;

                        if (response.IsSuccessStatusCode)
                        {
                            Label1.Visible = true;
                            Label1.ForeColor = System.Drawing.Color.Green;
                            Label1.Text = "Success to send email.";
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                        }
                        else
                        {
                            Label1.Visible = true;
                            Label1.ForeColor = System.Drawing.Color.Red;
                            Label1.Text = "Failed to send email.";
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Label1.Visible = true;
                    Label1.Text = ex.Message;
                }
            }
        }

        public void verification()
        {
            try
            {
                string email = TextBoxEnter.Text;
                string uri = "http://localhost:9999/";
                string parameter = "users/cariUsers?email=" + email + "";
                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri(uri);
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = clients.GetAsync(parameter).Result;

                if (response.IsSuccessStatusCode)
                {
                    var content = response.Content.ReadAsStringAsync().Result;
                    var userdata = JsonConvert.DeserializeObject<Users>(content);

                    bool activated = userdata.activated;
                    if (activated == true)
                    {
                        sendemailuseractive();
                        TextBoxEnter.Text = string.Empty;
                    }
                    else
                    {
                        sendemailuserinactive();
                        TextBoxEnter.Text = string.Empty;
                    }

                }
                else
                {
                    Label1.Visible = true;
                    Label1.ForeColor = System.Drawing.Color.Red;
                    Label1.Text = "User is not found.";
                }
            }
            catch (Exception ex)
            {
                Label1.Visible = true;
                Label1.ForeColor = System.Drawing.Color.Red;
                Label1.Text = ex.Message;
            }
        }

        protected void ButtonSearch_Click(object sender, EventArgs e)
        {
            verification();
        }
    }
}