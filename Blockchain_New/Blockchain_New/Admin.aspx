﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterpageSuperadmin.Master" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="Blockchain_New.Admin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Admin</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <link href="Generalstyle.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link rel="Icon" href="Aset/favicon.png" type="image/png" /> 
    <script src="https://kit.fontawesome.com/16b5cf1a92.js" crossorigin:"anonymous"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<center><h3>List Admin</h3></center>
    <asp:GridView ID="DataUser" CssClass="mydata" runat="server" AutoGenerateColumns="False">
					<Columns>
                        <asp:BoundField DataField="email" HeaderText="E-mail Address" />
                        <asp:BoundField DataField="roleName" HeaderText="Role" />
                        <asp:BoundField DataField="registeredDate" HeaderText="Registered Date"  HtmlEncode="false" DataFormatString="{0:MM/dd/yyyy}" />
                        <asp:BoundField DataField="registeredTime" HeaderText="Registered Time"  HtmlEncode="false" DataFormatString="{0:HH:mm:ss tt}" />					
                        <asp:BoundField DataField="isActive" HeaderText="Is Active" />                      
                        <asp:TemplateField>
							<ItemTemplate>
							   <asp:LinkButton runat="server" CssClass="mydetail" ID="Delete" Text="Delete" OnClick = "Delete_Click"></asp:LinkButton>
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
    </asp:GridView>
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
</asp:Content>
