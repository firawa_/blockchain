﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;
using Blockchain_New.Models;
using System.Net.Http.Formatting;
using System.Collections.Generic;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Web.UI.WebControls;
using System.Runtime.CompilerServices;
using System.Drawing;
using System.IO;
using System.Data;
using Npgsql;

namespace Blockchain_New
{
    public partial class Backup : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["email"] == null && Session["role"] == null)
            {
                logout();
            }
            else if (Session["role"].ToString() == "USER")
            {
                Response.Redirect("Home.aspx");
            }
            else if (Session["role"].ToString() == "SUPERADMIN")
            {
                Response.Redirect("Home3.aspx");
            }
            else if (Session["role"].ToString() == "INSTANSI")
            {
                Response.Redirect("Home4.aspx");
            }
            else if (Session["role"].ToString() == "PERUSAHAAN")
            {
                Response.Redirect("Homeperusahaaan.aspx");
            }
            else
            {
                loadmygrid();
            }        
        }

        public void logout()
        {
            string email = GlobalVariable.email;
            try
            {
                using (var client = new HttpClient())
                {
                    Users logout = new Users
                    {
                        email = email
                    };

                    client.BaseAddress = new Uri("http://localhost:9999/users/");
                    var response = client.PostAsJsonAsync("logout", logout).Result;
                    var content = response.Content.ReadAsStringAsync().Result;

                    if (response.IsSuccessStatusCode)
                    {
                        Session.Abandon();
                        Response.Redirect("Login.aspx");
                    }

                }
            }
            catch (Exception ex)
            {

            }
        }

        public void loadmygrid()
        {
            string ConString = String.Format("Server=45.13.132.196;Port=5432;User Id=postgres;password=edu2019jakarta;Database=blockchain_dev");
            NpgsqlConnection con = new NpgsqlConnection(ConString);
            con.Open();
            string sql = "SELECT * FROM kelulusan";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
            NpgsqlDataAdapter da = new NpgsqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            Data.DataSource = dt;
            Data.DataBind();
            con.Close();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri("http://localhost:5000/");
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = clients.GetAsync("backupdata").Result;

                if (response.IsSuccessStatusCode)
                {
                    Labelberhasil.Visible = true;
                    Labelberhasil.Text = "Success to back up data.";
                    loadmygrid();
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    Certif.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Labelberhasil.Visible = true;
                Labelberhasil.Text = ex.Message;
            }
        }

        protected void Generate_Click(object Sender, EventArgs e)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    LinkButton Generate = Sender as LinkButton;

                    GridViewRow record = Generate.NamingContainer as GridViewRow;

                    string a = string.Format("{0:0.#}", float.Parse(record.Cells[3].Text));
                    float Score = float.Parse(a);

                    Certifikat cert = new Certifikat
                    {
                        name = record.Cells[0].Text,
                        email = record.Cells[1].Text,
                        test_name = record.Cells[2].Text,
                        score =  Score,
                        test_date = DateTime.Parse(record.Cells[4].Text).Date,
                        semester = int.Parse(record.Cells[5].Text),
                        sks = int.Parse(record.Cells[6].Text)
                    };

                    client.BaseAddress = new Uri("http://localhost:9999/cert/");
                    var response = client.PostAsJsonAsync("getHD", cert).Result;
                    var content = response.Content.ReadAsStringAsync().Result;

                    if (response.IsSuccessStatusCode)
                    {
                        string date = record.Cells[4].Text;

                        string fileName = record.Cells[0].Text + "-" + record.Cells[2].Text + "-" + date.Replace("/", "") + ".jpg";

                        string sourcePath = Server.MapPath("~/API");
                        string targetPath = Server.MapPath("~/Upload/Image");

                        string sourceFile = Path.Combine(sourcePath, fileName);
                        string destFile = Path.Combine(targetPath, fileName);

                        if (!Directory.Exists(targetPath))
                        {
                            Directory.CreateDirectory(targetPath);
                        }
                        else if (File.Exists(Server.MapPath("~/Upload/Image/" + fileName)))
                        {
                            File.Delete(Server.MapPath("~/Upload/Image/" + fileName));
                        }

                        File.Move(sourceFile, destFile);

                        Certif.Visible = true;
                        Certif.ImageUrl = "~/Upload/Image/" + fileName;

                        FileStream file = new FileStream(MapPath("~/Upload/Image/" + fileName), FileMode.Open, FileAccess.Read, FileShare.Read);
                        System.Drawing.Image img = System.Drawing.Image.FromStream(file);

                        if (img.Width > img.Height)
                        {
                            Certif.Width = 842;
                            Certif.Height = 595;
                        }
                        else if (img.Width < img.Height)
                        {
                            Certif.Width = 595;
                            Certif.Height = 842;
                        }
                        else
                        {
                            Certif.Width = 200;
                            Certif.Height = 200;
                        }

                        Labelberhasil.Visible = true;
                        Labelberhasil.Text = "Success to get Certificate.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else
                    {
                        Labelberhasil.Visible = true;
                        Labelberhasil.Text = "Failed";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                };
            }
            catch (Exception ex)
            {
                Labelberhasil.Visible = true;
                Labelberhasil.Text = ex.Message;
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
            }
        }
    }
}