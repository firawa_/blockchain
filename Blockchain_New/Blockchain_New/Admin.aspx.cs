﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;
using Blockchain_New.Models;
using System.Net.Http.Formatting;
using System.Collections.Generic;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Web.UI.WebControls;
using System.Runtime.CompilerServices;

namespace Blockchain_New
{
    public partial class Admin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["email"] == null && Session["role"] == null)
            {
                logout();
            }
            else if (Session["role"].ToString() == "USER")
            {
                Response.Redirect("Home.aspx");
            }
            else if (Session["role"].ToString() == "ADMIN")
            {
                Response.Redirect("Home2.aspx");
            }
            else if (Session["role"].ToString() == "INSTANSI")
            {
                Response.Redirect("Home4.aspx");
            }
            else if (Session["role"].ToString() == "PERUSAHAAN")
            {
                Response.Redirect("Homeperusahaaan.aspx");
            }
            else
            {
                loadmygrid();
            }    
        }

        public void logout()
        {
            string email = GlobalVariable.email;
            try
            {
                using (var client = new HttpClient())
                {
                    Users logout = new Users
                    {
                        email = email
                    };

                    client.BaseAddress = new Uri("http://localhost:9999/users/");
                    var response = client.PostAsJsonAsync("logout", logout).Result;
                    var content = response.Content.ReadAsStringAsync().Result;

                    if (response.IsSuccessStatusCode)
                    {
                        Session.Abandon();
                        Response.Redirect("Login.aspx");
                    }

                }
            }
            catch (Exception ex)
            {

            }
        }

        public void loadmygrid()
        {
            string email = Session["email"].ToString();

            if (email != null)
            {
                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri("http://localhost:9999/admin/");
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = clients.GetAsync("daftarAdmin").Result;

                if (response.IsSuccessStatusCode)
                {
                    var content = response.Content.ReadAsStringAsync().Result;
                    DataUser.DataSource = (new JavaScriptSerializer()).Deserialize<List<Users>>(content);
                    DataUser.DataBind();
                }
            }
        }

        protected void Delete_Click(object Sender, EventArgs e)
        {
            LinkButton detailbutton = Sender as LinkButton;

            GridViewRow record = detailbutton.NamingContainer as GridViewRow;

            string email = record.Cells[0].Text;

            try
            {
                using (var client = new HttpClient())
                {
                    Users user = new Users
                    {
                        email = email
                    };

                    client.BaseAddress = new Uri("http://localhost:9999/admin/");
                    var response = client.PostAsJsonAsync("delete", user).Result;
                    var content = response.Content.ReadAsStringAsync().Result;

                    if (response.IsSuccessStatusCode)
                    {
                        loadmygrid();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}